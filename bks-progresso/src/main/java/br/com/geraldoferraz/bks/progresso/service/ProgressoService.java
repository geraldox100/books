package br.com.geraldoferraz.bks.progresso.service;

import static br.com.geraldoferraz.bks.comum.util.Assercoes.notNull;
import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustNotBeNull;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.geraldoferraz.bks.entidades.Progresso;
import br.com.geraldoferraz.bks.progresso.exceptions.ProgressoNaoEncontradoException;
import br.com.geraldoferraz.bks.progresso.repository.ProgressoRepository;

@Service
public class ProgressoService {

	@Autowired
	private ProgressoRepository repo;

	public List<Progresso> list() {
		return repo.findAll();
	}

	public Progresso create(Progresso progresso) {
		validarCamposObrigatorios(progresso);
		return repo.save(progresso);
	}

	public Progresso retrieve(Long id) {
		validarId(id);
		return repo.findById(id).orElseThrow(() -> new ProgressoNaoEncontradoException(id));
	}

	public Progresso update(Long id, Progresso alteracao) {
		validarId(id);
		validarProgresso(alteracao);

		Progresso progresso = repo.findById(id).orElseThrow(() -> new ProgressoNaoEncontradoException(id));

		if (notNull(alteracao.getLivro())) {
			progresso.setLivro(alteracao.getLivro());
		}

		if (notNull(alteracao.getDataHora())) {
			progresso.setDataHora(alteracao.getDataHora());
		}

		if (notNull(alteracao.getQuantidade())) {
			progresso.setQuantidade(alteracao.getQuantidade());
		}

		validarCamposObrigatorios(progresso);

		return repo.save(progresso);
	}

	public void delete(Long id) {
		if (repo.existsById(id)) {
			repo.deleteById(id);
		} else {
			throw new ProgressoNaoEncontradoException(id);
		}
	}

	private void validarId(Long id) {
		mustNotBeNull(id, "id");
	}

	private void validarProgresso(Progresso progresso) {
		mustNotBeNull(progresso, "progresso");
	}

	private void validarCamposObrigatorios(Progresso progresso) {
		mustNotBeNull(progresso, "progresso");
		mustNotBeNull(progresso.getDataHora(), "data hora");
		mustNotBeNull(progresso.getQuantidade(), "quantidade");
		mustNotBeNull(progresso.getLivro(), "livro");
		mustNotBeNull(progresso.getLivro().getId(), "id livro");
	}

}
