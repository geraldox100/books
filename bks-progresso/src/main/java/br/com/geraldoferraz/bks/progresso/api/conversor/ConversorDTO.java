package br.com.geraldoferraz.bks.progresso.api.conversor;

import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustNotBeNull;

import br.com.geraldoferraz.bks.entidades.LivroComprado;
import br.com.geraldoferraz.bks.entidades.Progresso;
import br.com.geraldoferraz.bks.progresso.api.dto.ProgressoDTO;

public class ConversorDTO {

	private ProgressoDTO dto;

	ConversorDTO(ProgressoDTO dto) {
		mustNotBeNull(dto,"ProgressoDTO");
		this.dto = dto;
	}

	public Progresso converter() {
		Progresso retorno = new Progresso();
		
		retorno.setId(dto.getId());
		retorno.setDataHora(dto.getDataHora());
		retorno.setQuantidade(dto.getQuantidade());
		LivroComprado livro = new LivroComprado();
		livro.setId(dto.getIdLivroComprado());
		retorno.setLivro(livro);
		
		return retorno;
	}

}
