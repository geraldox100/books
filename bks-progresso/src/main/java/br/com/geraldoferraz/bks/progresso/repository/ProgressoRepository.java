package br.com.geraldoferraz.bks.progresso.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.geraldoferraz.bks.entidades.Progresso;

public interface ProgressoRepository extends JpaRepository<Progresso, Long> {

}
