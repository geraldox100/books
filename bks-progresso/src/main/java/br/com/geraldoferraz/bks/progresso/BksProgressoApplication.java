package br.com.geraldoferraz.bks.progresso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan("br.com.geraldoferraz.bks.entidades")
public class BksProgressoApplication {
	private BksProgressoApplication() {}
	
	private static SpringApplication app = new SpringApplication(BksProgressoApplication.class);

	public static void main(String[] args) {
		app.run();
	}

}
