package br.com.geraldoferraz.bks.progresso.exceptions;

import br.com.geraldoferraz.bks.comum.exceptions.RecursoNaoEncontradoException;

public class ProgressoNaoEncontradoException extends RecursoNaoEncontradoException {

	public static final String MENSAGEM_PADRAO = "Progresso não encontrado";
	private static final long serialVersionUID = 1L;
	
	public ProgressoNaoEncontradoException(String mensagem) {
		super(mensagem);
	}

	public ProgressoNaoEncontradoException(Long id) {
		super("Progresso não encontrado para id: " + id);
	}
	
	public ProgressoNaoEncontradoException() {
		super(MENSAGEM_PADRAO);
	}

	public static void throwMe(Long id) {
		throw new ProgressoNaoEncontradoException(id);
	}

}