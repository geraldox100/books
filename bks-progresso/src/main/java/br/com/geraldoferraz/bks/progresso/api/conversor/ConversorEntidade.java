package br.com.geraldoferraz.bks.progresso.api.conversor;

import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustNotBeNull;

import java.util.ArrayList;
import java.util.List;

import br.com.geraldoferraz.bks.entidades.Progresso;
import br.com.geraldoferraz.bks.progresso.api.dto.ProgressoDTO;

public class ConversorEntidade {

	private List<Progresso> entidades;
	private Progresso entidade;

	ConversorEntidade(List<Progresso> entidades) {
		mustNotBeNull(entidades, "entidades");
		this.entidades = entidades;
	}

	public ConversorEntidade(Progresso entidade) {
		mustNotBeNull(entidade, "entidade");
		this.entidade = entidade;
	}

	public List<ProgressoDTO> converterLista() {
		List<ProgressoDTO> retorno = new ArrayList<>();

		for (Progresso e : entidades) {
			retorno.add(converter(e));
		}

		return retorno;
	}

	public ProgressoDTO converter() {
		return converter(this.entidade);
	}

	public ProgressoDTO converter(Progresso entidade) {
		ProgressoDTO retorno = new ProgressoDTO();
		retorno.setId(entidade.getId());
		retorno.setDataHora(entidade.getDataHora());
		retorno.setQuantidade(entidade.getQuantidade());
		retorno.setIdLivroComprado(entidade.getLivro().getId());
		return retorno;
	}

}
