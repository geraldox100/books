package br.com.geraldoferraz.bks.progresso.api.dto;

import java.io.Serializable;
import java.util.Date;

public class ProgressoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Integer quantidade;
	private Date dataHora;
	private Long idLivroComprado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public Long getIdLivroComprado() {
		return idLivroComprado;
	}

	public void setIdLivroComprado(Long idLivroComprado) {
		this.idLivroComprado = idLivroComprado;
	}

}
