package br.com.geraldoferraz.bks.progresso.api.conversor;

import java.util.List;

import br.com.geraldoferraz.bks.entidades.Progresso;
import br.com.geraldoferraz.bks.progresso.api.dto.ProgressoDTO;

public class Conversor {

	public ConversorEntidade entidades(List<Progresso> entidades) {
		return new ConversorEntidade(entidades);
	}

	public ConversorEntidade entidade(Progresso entidade) {
		return new ConversorEntidade(entidade);
	}
	
	public ConversorDTO dto(ProgressoDTO dto) {
		return new ConversorDTO(dto);
	}


}
