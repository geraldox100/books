package br.com.geraldoferraz.bks.progresso.api.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.geraldoferraz.bks.entidades.Progresso;
import br.com.geraldoferraz.bks.progresso.api.conversor.Conversor;
import br.com.geraldoferraz.bks.progresso.api.dto.ProgressoDTO;
import br.com.geraldoferraz.bks.progresso.service.ProgressoService;

@RestController
@RequestMapping("/progresso")
public class ProgressoController {

	@Autowired
	private ProgressoService service;

	@Autowired
	private Conversor conversor;

	@GetMapping("/")
	public List<ProgressoDTO> list() {
		return conversor.entidades(service.list()).converterLista();
	}

	@PostMapping("/")
	public ResponseEntity<ProgressoDTO> create(@RequestBody ProgressoDTO dto, UriComponentsBuilder uriBuilder) {
		Progresso entidade = conversor.dto(dto).converter();
		entidade = service.create(entidade);

		URI uri = uriBuilder.path("/progresso/{id}").buildAndExpand(entidade.getId()).toUri();

		return ResponseEntity.created(uri).body(conversor.entidade(entidade).converter());
	}

	@GetMapping("/{id}")
	public ResponseEntity<ProgressoDTO> retrieve(@PathVariable Long id) {
		ProgressoDTO dto = conversor.entidade(service.retrieve(id)).converter();
		return ResponseEntity.ok(dto);
	}

	@PutMapping("/{id}")
	public ResponseEntity<ProgressoDTO> update(@PathVariable Long id, @RequestBody ProgressoDTO dto) {
		Progresso entidade = conversor.dto(dto).converter();
		return ResponseEntity.ok(conversor.entidade(service.update(id, entidade)).converter());
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> delete(@PathVariable Long id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}

}
