package br.com.geraldoferraz.bks.progresso.api.controller;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.geraldoferraz.bks.entidades.LivroComprado;
import br.com.geraldoferraz.bks.entidades.Progresso;
import br.com.geraldoferraz.bks.progresso.api.conversor.Conversor;
import br.com.geraldoferraz.bks.progresso.api.conversor.ConversorDTO;
import br.com.geraldoferraz.bks.progresso.api.conversor.ConversorEntidade;
import br.com.geraldoferraz.bks.progresso.api.dto.ProgressoDTO;
import br.com.geraldoferraz.bks.progresso.service.ProgressoService;

@ExtendWith(MockitoExtension.class)
public class ProgressoControllerTest {
	@Mock
	private ProgressoService service;

	@Mock
	private Conversor conversor;

	@Mock
	private ConversorEntidade conversorEntidade;

	@Mock
	private ConversorDTO conversorDTO;

	@Spy
	private UriComponentsBuilder uriBuilder;

	@InjectMocks
	private ProgressoController controller;

	@BeforeEach
	public void beforeEach() {
		lenient().when(conversor.entidade(Mockito.any())).thenReturn(conversorEntidade);
		lenient().when(conversor.entidades(Mockito.anyList())).thenReturn(conversorEntidade);
		lenient().when(conversor.dto(Mockito.any(ProgressoDTO.class))).thenReturn(conversorDTO);
	}

	@Test
	public void quandoExecutarList() {
		controller.list();
		verify(service, times(1)).list();
		verify(conversorEntidade, times(1)).converterLista();
	}

	@Test
	public void quandoExecutarCreate() {
		ProgressoDTO dto = criarDTO();
		Progresso entidade = criarEntidade();

		when(conversorDTO.converter()).thenReturn(entidade);
		when(service.create(Mockito.any())).thenReturn(entidade);
		when(conversorEntidade.converter()).thenReturn(dto);

		ResponseEntity<ProgressoDTO> response = controller.create(dto, uriBuilder);

		verify(service, times(1)).create(entidade);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertEquals(dto, response.getBody());
	}

	@Test
	public void quandoExecutarRetrieve() {
		ProgressoDTO dto = criarDTO();
		when(conversorEntidade.converter()).thenReturn(dto);

		ResponseEntity<ProgressoDTO> response = controller.retrieve(1l);

		verify(service, times(1)).retrieve(1l);
		verify(conversorEntidade, times(1)).converter();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(dto, response.getBody());
	}

	@Test
	public void quandoExecutarUpdate() {
		ProgressoDTO dto = criarDTO();
		Progresso entidade = criarEntidade();

		when(conversorDTO.converter()).thenReturn(entidade);
		when(conversorEntidade.converter()).thenReturn(dto);

		ResponseEntity<ProgressoDTO> response = controller.update(1l, dto);

		verify(service, times(1)).update(1l, entidade);
		verify(conversorEntidade, times(1)).converter();
		verify(conversorDTO, times(1)).converter();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(dto, response.getBody());
	}

	@Test
	public void quandoExecutarDelete() {
		ResponseEntity<Object> response = controller.delete(1l);
		verify(service, times(1)).delete(1l);

		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
		assertNull(response.getBody());
	}

	private ProgressoDTO criarDTO() {
		ProgressoDTO dto = new ProgressoDTO();
		dto.setId(1l);
		dto.setQuantidade(100);
		dto.setDataHora(new Date());
		dto.setIdLivroComprado(2l);
		return dto;
	}

	private Progresso criarEntidade() {
		Progresso entidade = new Progresso();
		entidade.setId(1l);
		entidade.setQuantidade(100);
		entidade.setDataHora(new Date());
		entidade.setLivro(new LivroComprado());
		return entidade;
	}
}
