package br.com.geraldoferraz.bks.progresso.api.conversor;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.geraldoferraz.bks.entidades.LivroComprado;
import br.com.geraldoferraz.bks.entidades.Progresso;
import br.com.geraldoferraz.bks.progresso.api.dto.ProgressoDTO;

@ExtendWith(MockitoExtension.class)
public class ConversorEntidadeTest {
	
		private ConversorEntidade conversor;

		@Nested
		class QuandoConstruir {

			@Test
			public void passandoEntidadeNula() {
				Progresso progresso = null;
				IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
						() -> new ConversorEntidade(progresso));
				assertEquals("Campo entidade não pode ser nulo", e.getMessage());
			}

			@Test
			public void passandoListaNula() {
				List<Progresso> progressos = null;
				IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
						() -> new ConversorEntidade(progressos));
				assertEquals("Campo entidades não pode ser nulo", e.getMessage());
			}
		}
		
		@Nested
		class QuandoConverterLista{
			@Test
			public void quandoConverterListaComListaVazia() {
				conversor = new ConversorEntidade(new ArrayList<>());
				
				List<ProgressoDTO> lista = conversor.converterLista();
				assertNotNull(lista);
				assertEquals(0, lista.size());
				
			}
			
			@Test
			public void quandoConverterListaComUmElemento() {
				ArrayList<Progresso> entidades = new ArrayList<>();
				entidades.add(criarLivro());
				
				Date dataCompra = new Date();
				for (Progresso progresso : entidades) {
					progresso.setDataHora(dataCompra);
				}
				
				conversor = new ConversorEntidade(entidades);
				
				List<ProgressoDTO> lista = conversor.converterLista();
				assertEquals(1, lista.size());
				
				ProgressoDTO dto = lista.get(0);
				
				assertAll("progressoDTO", 
						() -> assertEquals(1l, dto.getId()),
						() -> assertEquals(100, dto.getQuantidade()),
						() -> assertEquals(dataCompra, dto.getDataHora()), 
						() -> assertEquals(2l, dto.getIdLivroComprado()));
				
			}
			
			@Test
			public void quandoConverterListaVariosElementos() {
				ArrayList<Progresso> entidades = new ArrayList<>();

				entidades.add(criarLivro());
				entidades.add(criarLivro());
				entidades.add(criarLivro());
				
				Date dataCompra = new Date();
				for (Progresso progresso : entidades) {
					progresso.setDataHora(dataCompra);
				}
				
				conversor = new ConversorEntidade(entidades);
				
				List<ProgressoDTO> lista = conversor.converterLista();
				assertEquals(3, lista.size());
				
				for (ProgressoDTO dto : lista) {
					assertAll("progressoDTO", 
							() -> assertEquals(1l, dto.getId()),
							() -> assertEquals(100, dto.getQuantidade()),
							() -> assertEquals(dataCompra, dto.getDataHora()), 
							() -> assertEquals(2l, dto.getIdLivroComprado()));
				}
				
			}
		}
		
		@Nested
		class QuandoConverterEntidade{
			@Test
			public void passandoUmElementoDeveConstruirListaComUmElemento() {
				Progresso progresso = criarLivro();
				
				Date dataCompra = new Date();
				progresso.setDataHora(dataCompra);
				
				conversor = new ConversorEntidade(progresso);
				
				ProgressoDTO dto = conversor.converter();
				
				assertAll("progressoDTO", 
						() -> assertEquals(1l, dto.getId()),
						() -> assertEquals(100, dto.getQuantidade()),
						() -> assertEquals(dataCompra, dto.getDataHora()), 
						() -> assertEquals(2l, dto.getIdLivroComprado()));
				
			}
		}
		
		private Progresso criarLivro() {
			Progresso progresso = new Progresso();
			progresso.setId(1l);
			progresso.setQuantidade(100);
			progresso.setDataHora(new Date());
			LivroComprado livro = new LivroComprado();
			livro.setId(2l);
			progresso.setLivro(livro);
			return progresso;
		}
	}