package br.com.geraldoferraz.bks.progresso.exceptions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ProgressoNaoEncontradoExceptionTest {
	@Nested
	class QuandoConstruirProgressoNaoEncontradoException {

		@Test
		public void comConstrutorPadraoDeveConstruirMensagemPadrao() {
			ProgressoNaoEncontradoException e = new ProgressoNaoEncontradoException();
			assertEquals(ProgressoNaoEncontradoException.MENSAGEM_PADRAO, e.getMessage());
		}

		@Test
		public void passandoMensagemDeveConstruirMensagemComTextoParametrizada() {
			String mensagem = "mensagem parametrizada";
			ProgressoNaoEncontradoException e = new ProgressoNaoEncontradoException(mensagem);
			assertEquals(mensagem, e.getMessage());
		}

		@Test
		public void passandoIdDeveConstruirMensagemComIdParametrizado() {
			ProgressoNaoEncontradoException e = new ProgressoNaoEncontradoException(2l);
			assertEquals("Progresso não encontrado para id: 2", e.getMessage());
		}

	}

	@Nested
	class QuandoExecutarThrowMe {
		@Test
		public void deveLancarExcecao() {
			Assertions.assertThrows(ProgressoNaoEncontradoException.class,
					() -> ProgressoNaoEncontradoException.throwMe(3l));
		}
	}
}
