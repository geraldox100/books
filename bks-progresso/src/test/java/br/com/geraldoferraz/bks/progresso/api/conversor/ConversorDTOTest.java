package br.com.geraldoferraz.bks.progresso.api.conversor;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Date;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import br.com.geraldoferraz.bks.entidades.Progresso;
import br.com.geraldoferraz.bks.progresso.api.dto.ProgressoDTO;

public class ConversorDTOTest {

	@Test
	public void quandoConstruirPassandoEntidadeNula() {
		ProgressoDTO progresso = null;
		IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> new ConversorDTO(progresso));
		assertEquals("Campo ProgressoDTO não pode ser nulo", e.getMessage());

	}
	
	@Nested
	class QuandoConverterDTO{
		@Test
		public void quandoConverterListaComUmElemento() {
			ProgressoDTO progresso = criarProgresso();
			
			Date dataInicio = new Date();
			progresso.setDataHora(dataInicio);
			
			ConversorDTO conversor = new ConversorDTO(progresso);
			
			Progresso dto = conversor.converter();
			
			assertAll("progresso", 
					() -> assertEquals(1l, dto.getId()),
					() -> assertEquals(2l, dto.getLivro().getId()),
					() -> assertEquals(100, dto.getQuantidade()),
					() -> assertEquals(dataInicio, dto.getDataHora()));
			
		}
	}
	
	private ProgressoDTO criarProgresso() {
		ProgressoDTO progresso = new ProgressoDTO();
		progresso.setId(1l);
		progresso.setIdLivroComprado(2l);
		progresso.setQuantidade(100);
		progresso.setDataHora(new Date());
		return progresso;
	}
}
