package br.com.geraldoferraz.bks.progresso.service;

import static org.junit.Assert.assertSame;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.geraldoferraz.bks.entidades.LivroComprado;
import br.com.geraldoferraz.bks.entidades.Progresso;
import br.com.geraldoferraz.bks.progresso.exceptions.ProgressoNaoEncontradoException;
import br.com.geraldoferraz.bks.progresso.repository.ProgressoRepository;
import br.com.geraldoferraz.bks.progresso.service.ProgressoService;

@ExtendWith(MockitoExtension.class)
public class ProgressoServiceTest {

	@Mock
	private ProgressoRepository repo;

	@InjectMocks
	private ProgressoService service;

	@Nested
	class QuandoExecutarLista {
		@Test
		public void deveRetornarLista() {
			service.list();
			verify(repo, times(1)).findAll();
		}
	}

	@Nested
	class DeveLancarExcecaoQuandoExecutarCreate {
		@Test
		public void passandoProgressoNulaDeveLancarExcecao() {
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.create(null));
			assertEquals("Campo progresso não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoDataHoraNulaDeveLancarExcecao() {
			Progresso progresso = criarProgresso();
			progresso.setDataHora(null);
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.create(progresso));
			assertEquals("Campo data hora não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoQuantidadeNulaDeveLancarExcecao() {
			Progresso progresso = criarProgresso();
			progresso.setQuantidade(null);
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.create(progresso));
			assertEquals("Campo quantidade não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoLivroNulaDeveLancarExcecao() {
			Progresso progresso = criarProgresso();
			progresso.setLivro(null);
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.create(progresso));
			assertEquals("Campo livro não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoProgressoCompletaDevePersistirChamandoRepo() {
			Progresso progresso = criarProgresso();
			service.create(progresso);
			verify(repo, times(1)).save(progresso);
		}

	}

	@Nested
	class QuandoExecutarRetrieve {
		@Test
		public void passandoIdNullDeveLancarExcecao() {
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.retrieve(null));
			assertEquals("Campo id não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoIdQueNaoExisteDeveLancarExcecao() {
			when(repo.findById(1l)).thenReturn(java.util.Optional.empty());
			assertThrows(ProgressoNaoEncontradoException.class, () -> service.retrieve(1l));
		}

		@Test
		public void passandoIdValidoDeveRetornarObjetoEncontrado() {
			Progresso progresso = criarProgresso();
			when(repo.findById(1l)).thenReturn(java.util.Optional.of(progresso));
			Progresso progressoEncontrada = service.retrieve(1l);
			assertSame(progresso, progressoEncontrada);
		}
	}

	@Nested
	class QuandoExecutarUpdate {
		@Test
		public void passandoIdNullDeveLancarExcecao() {
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
					() -> service.update(null, criarProgresso()));
			assertEquals("Campo id não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoProgressoNullDeveLancarExcecao() {
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.update(2l, null));
			assertEquals("Campo progresso não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoIdQueNaoExisteDeveLancarExcecao() {
			when(repo.findById(2l)).thenReturn(Optional.empty());
			ProgressoNaoEncontradoException e = assertThrows(ProgressoNaoEncontradoException.class,
					() -> service.update(2l, criarProgresso()));
			assertEquals("Progresso não encontrado para id: 2", e.getMessage());
		}

		@Test
		public void quandoPassarAlteracaoDeApenasUmCampoValidaDeveSalvar() {
			Progresso progresso = criarProgresso();
			when(repo.findById(2l)).thenReturn(java.util.Optional.of(progresso));
			when(repo.save(progresso)).thenReturn(progresso);

			Progresso alteracao = new Progresso();
			alteracao.setQuantidade(500);

			Progresso produto = service.update(2l, alteracao);

			verify(repo, times(1)).save(progresso);

			assertSame(progresso, produto);
			assertEquals(alteracao.getQuantidade(), produto.getQuantidade());
			assertEquals(progresso.getId(), produto.getId());
			assertEquals(progresso.getDataHora(), produto.getDataHora());
			assertEquals(progresso.getLivro(), produto.getLivro());
		}

		@Test
		public void quandoPassarAlteracaoDeTodosOsCampoValidaDeveSalvar() {
			Progresso progresso = criarProgresso();
			when(repo.findById(2l)).thenReturn(java.util.Optional.of(progresso));
			when(repo.save(progresso)).thenReturn(progresso);

			Progresso alteracao = new Progresso();
			LivroComprado livro = new LivroComprado();
			livro.setId(30l);
			alteracao.setLivro(livro);
			alteracao.setDataHora(new Date());
			alteracao.setQuantidade(33);

			Progresso produto = service.update(2l, alteracao);

			verify(repo, times(1)).save(progresso);

			assertSame(progresso, produto);
			assertEquals(alteracao.getLivro(), produto.getLivro());
			assertEquals(progresso.getId(), produto.getId());
			assertEquals(alteracao.getDataHora(), produto.getDataHora());
			assertEquals(alteracao.getQuantidade(), produto.getQuantidade());
		}

		@Test
		public void quandoPassarApenasTituloNuloNaoDeveAlterar() {
			Progresso progresso = criarProgresso();
			when(repo.findById(2l)).thenReturn(java.util.Optional.of(progresso));
			when(repo.save(progresso)).thenReturn(progresso);

			Progresso alteracao = new Progresso();
			alteracao.setLivro(null);
			Progresso produto = service.update(2l, alteracao);

			assertSame(progresso, produto);
			assertEquals(progresso.getLivro(), produto.getLivro());
		}

	}

	@Nested
	class QuandoExecutarDelete {
		@Test
		public void quandoPassarIdQueNaoExisteDeveLancarExcecao() {
			when(repo.existsById(2l)).thenReturn(false);
			Mockito.verify(repo, never()).deleteById(1l);
			ProgressoNaoEncontradoException e = assertThrows(ProgressoNaoEncontradoException.class,
					() -> service.delete(2l));
			assertEquals("Progresso não encontrado para id: 2", e.getMessage());
		}

		@Test
		public void quandoPassarIdQueExisteDeveDeletar() {
			when(repo.existsById(1l)).thenReturn(true);
			service.delete(1l);
			Mockito.verify(repo, times(1)).deleteById(1l);
		}
	}

	private Progresso criarProgresso() {
		Progresso entidade = new Progresso();
		entidade.setId(1l);
		entidade.setDataHora(new Date());
		entidade.setQuantidade(100);
		LivroComprado livro = new LivroComprado();
		livro.setId(2l);
		entidade.setLivro(livro);
		return entidade;
	}
}
