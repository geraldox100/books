package br.com.geraldoferraz.bks.progresso;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.SpringApplication;

import br.com.geraldoferraz.bks.comum.test.Injector;

/**
 * Este teste não tem nenhum propósito. Foi feito por diversão.
 * 
 * @author Geraldo.Filho
 *
 */
@ExtendWith(MockitoExtension.class)
public class BksProgressoApplicationTest {

	@Mock
	private SpringApplication springApp;

	@Test
	public void aoIniciarAplicacaoDeveExecutarSpringRun() throws Exception {
		Injector.inject(BksProgressoApplication.class, "app", springApp);
		BksProgressoApplication.main(new String[] {});
		Mockito.verify(springApp, Mockito.times(1)).run(Mockito.any());
	}

}
