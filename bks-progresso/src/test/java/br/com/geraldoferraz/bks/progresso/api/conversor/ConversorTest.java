package br.com.geraldoferraz.bks.progresso.api.conversor;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.com.geraldoferraz.bks.entidades.Progresso;
import br.com.geraldoferraz.bks.progresso.api.dto.ProgressoDTO;


public class ConversorTest {
	private Conversor conversor;

	@BeforeEach
	public void beforeEach() {
		conversor = new Conversor();
	}
	
	@Test
	public void quandoExecutarDTODeveRetornarInstanciaDeConversorDTO() {
		ConversorDTO dto = conversor.dto(new ProgressoDTO());
		assertNotNull(dto);
	}
	
	@Test
	public void quandoExecutarEntidadeDeveRetornarInstanciaDeConversorEntidade() {
		ConversorEntidade entidade = conversor.entidade(new Progresso());
		assertNotNull(entidade);
	}
	
	@Test
	public void quandoExecutarEntidadesDeveRetornarInstanciaDeConversorEntidade() {
		ConversorEntidade entidade = conversor.entidades(asList(new Progresso()));
		assertNotNull(entidade);
	}
}
