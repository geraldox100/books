package br.com.geraldoferraz.bks.compra.api.conversor;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import br.com.geraldoferraz.bks.compra.api.dto.LivroDTO;
import br.com.geraldoferraz.bks.entidades.Autor;
import br.com.geraldoferraz.bks.entidades.Categoria;
import br.com.geraldoferraz.bks.entidades.LivroComprado;


public class ConversorEntidadeTest {
	private ConversorEntidade conversor;

	@Nested
	class QuandoConstruir {

		@Test
		public void passandoEntidadeNula() {
			LivroComprado livro = null;
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
					() -> new ConversorEntidade(livro));
			assertEquals("Campo entidade não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoListaNula() {
			List<LivroComprado> livros = null;
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
					() -> new ConversorEntidade(livros));
			assertEquals("Campo entidades não pode ser nulo", e.getMessage());
		}
	}
	
	@Nested
	class QuandoConverterLista{
		@Test
		public void quandoConverterListaComListaVazia() {
			conversor = new ConversorEntidade(new ArrayList<>());
			
			List<LivroDTO> lista = conversor.converterLista();
			assertNotNull(lista);
			assertEquals(0, lista.size());
			
		}
		
		@Test
		public void quandoConverterListaComUmElemento() {
			ArrayList<LivroComprado> entidades = new ArrayList<>();
			entidades.add(criarLivro());
			
			Date dataCompra = new Date();
			for (LivroComprado livro : entidades) {
				livro.setDataCompra(dataCompra);
			}
			
			conversor = new ConversorEntidade(entidades);
			
			List<LivroDTO> lista = conversor.converterLista();
			assertEquals(1, lista.size());
			
			LivroDTO dto = lista.get(0);
			
			assertAll("livroDTO", 
					() -> assertEquals(1l, dto.getId()),
					() -> assertEquals("nome", dto.getNome()),
					() -> assertEquals(100, dto.getPaginas()),
					() -> assertEquals(dataCompra, dto.getDataCompra()), 
					() -> assertEquals("autor", dto.getAutor()),
					() -> assertEquals("categoria", dto.getCategoria()));
			
		}
		
		@Test
		public void quandoConverterListaVariosElementos() {
			ArrayList<LivroComprado> entidades = new ArrayList<>();

			entidades.add(criarLivro());
			entidades.add(criarLivro());
			entidades.add(criarLivro());
			
			Date dataCompra = new Date();
			for (LivroComprado livro : entidades) {
				livro.setDataCompra(dataCompra);
			}
			
			conversor = new ConversorEntidade(entidades);
			
			List<LivroDTO> lista = conversor.converterLista();
			assertEquals(3, lista.size());
			
			for (LivroDTO dto : lista) {
				assertAll("livroDTO", 
						() -> assertEquals(1l, dto.getId()),
						() -> assertEquals("nome", dto.getNome()),
						() -> assertEquals(100, dto.getPaginas()),
						() -> assertEquals(dataCompra, dto.getDataCompra()), 
						() -> assertEquals("autor", dto.getAutor()),
						() -> assertEquals("categoria", dto.getCategoria()));
			}
			
		}
	}
	
	@Nested
	class QuandoConverterEntidade{
		@Test
		public void passandoUmElementoDeveConstruirListaComUmElemento() {
			LivroComprado livro = criarLivro();
			
			Date dataCompra = new Date();
			livro.setDataCompra(dataCompra);
			
			conversor = new ConversorEntidade(livro);
			
			LivroDTO dto = conversor.converter();
			
			assertAll("livroDTO", 
					() -> assertEquals(1l, dto.getId()),
					() -> assertEquals("nome", dto.getNome()),
					() -> assertEquals(100, dto.getPaginas()),
					() -> assertEquals(dataCompra, dto.getDataCompra()), 
					() -> assertEquals("autor", dto.getAutor()),
					() -> assertEquals("categoria", dto.getCategoria()));
			
		}
	}
	
	private LivroComprado criarLivro() {
		LivroComprado livro = new LivroComprado();
		livro.setId(1l);
		livro.setNome("nome");
		livro.setAutor(new Autor("autor"));
		livro.setCategoria(new Categoria("categoria"));
		livro.setPaginas(100);
		livro.setDataCompra(new Date());
		return livro;
	}
}
