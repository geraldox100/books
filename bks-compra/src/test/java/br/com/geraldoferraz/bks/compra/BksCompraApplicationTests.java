package br.com.geraldoferraz.bks.compra;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.SpringApplication;

import br.com.geraldoferraz.bks.comum.test.Injector;

@ExtendWith(MockitoExtension.class)
class BksCompraApplicationTests {

	@Mock
	private SpringApplication springApp;

	@Test
	public void aoIniciarAplicacaoDeveExecutarSpringRun() throws Exception {
		Injector.inject(BksCompraApplication.class, "app", springApp);
		BksCompraApplication.main(new String[] {});
		Mockito.verify(springApp, Mockito.times(1)).run(Mockito.any());
	}

}
