package br.com.geraldoferraz.bks.compra.api.conversor;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.com.geraldoferraz.bks.compra.api.dto.LivroDTO;
import br.com.geraldoferraz.bks.entidades.LivroComprado;


public class ConversorTest {
	
	private Conversor conversor;

	@BeforeEach
	public void beforeEach() {
		conversor = new Conversor();
	}
	
	@Test
	public void quandoExecutarDTODeveRetornarInstanciaDeConversorDTO() {
		ConversorDTO dto = conversor.dto(new LivroDTO());
		assertNotNull(dto);
	}
	
	@Test
	public void quandoExecutarEntidadeDeveRetornarInstanciaDeConversorEntidade() {
		ConversorEntidade entidade = conversor.entidade(new LivroComprado());
		assertNotNull(entidade);
	}
	
	@Test
	public void quandoExecutarEntidadesDeveRetornarInstanciaDeConversorEntidade() {
		ConversorEntidade entidade = conversor.entidades(asList(new LivroComprado()));
		assertNotNull(entidade);
	}
}
