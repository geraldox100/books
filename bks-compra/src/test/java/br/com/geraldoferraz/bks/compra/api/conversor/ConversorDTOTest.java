package br.com.geraldoferraz.bks.compra.api.conversor;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Date;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import br.com.geraldoferraz.bks.compra.api.dto.LivroDTO;
import br.com.geraldoferraz.bks.entidades.LivroComprado;

public class ConversorDTOTest {
	@Test
	public void quandoConstruirPassandoEntidadeNula() {
		LivroDTO meta = null;
		IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> new ConversorDTO(meta));
		assertEquals("Campo dto não pode ser nulo", e.getMessage());

	}
	
	@Nested
	class QuandoConverterDTO{
		@Test
		public void quandoConverterPassandoDataCompra() {
			LivroDTO livro = criarLivro();
			
			Date dataCompra = new Date();
			livro.setDataCompra(dataCompra);
			
			ConversorDTO conversor = new ConversorDTO(livro);
			
			LivroComprado dto = conversor.converter();
			
			assertAll("meta", 
					() -> assertEquals("nome", dto.getNome()),
					() -> assertEquals(100, dto.getPaginas()),
					() -> assertEquals(dataCompra, dto.getDataCompra()), 
					() -> assertEquals("autor", dto.getAutor().getNome()),
					() -> assertEquals("categoria", dto.getCategoria().getNome()));
			
		}
		
		@Test
		public void quandoConverterSemPassarDataCompra() {
			LivroDTO livro = criarLivro();
			livro.setDataCompra(null);
			ConversorDTO conversor = new ConversorDTO(livro);
			
			LivroComprado dto = conversor.converter();
			
			assertAll("meta", 
					() -> assertEquals("nome", dto.getNome()),
					() -> assertEquals(100, dto.getPaginas()),
					() -> assertNotNull(dto.getDataCompra()), 
					() -> assertEquals("autor", dto.getAutor().getNome()),
					() -> assertEquals("categoria", dto.getCategoria().getNome()));
			
		}
	}
	
	private LivroDTO criarLivro() {
		LivroDTO livro = new LivroDTO();
		livro.setId(1l);
		livro.setNome("nome");
		livro.setAutor("autor");
		livro.setCategoria("categoria");
		livro.setPaginas(100);
		livro.setDataCompra(new Date());
		return livro;
	}
}
