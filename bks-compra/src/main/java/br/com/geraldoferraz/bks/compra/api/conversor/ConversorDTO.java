package br.com.geraldoferraz.bks.compra.api.conversor;

import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustNotBeNull;

import java.util.Date;

import br.com.geraldoferraz.bks.compra.api.dto.LivroDTO;
import br.com.geraldoferraz.bks.entidades.Autor;
import br.com.geraldoferraz.bks.entidades.Categoria;
import br.com.geraldoferraz.bks.entidades.LivroComprado;

public class ConversorDTO {
	
	private LivroDTO dto;
	
	ConversorDTO(LivroDTO dto) {
		mustNotBeNull(dto, "dto");
		this.dto = dto;
	}

	public LivroComprado converter() {
		LivroComprado entidade = new LivroComprado();
		
		entidade.setNome(dto.getNome());
		entidade.setPaginas(dto.getPaginas());
		
		entidade.setAutor(new Autor(dto.getAutor()));
		entidade.setCategoria(new Categoria(dto.getCategoria()));
		
		if(dto.getDataCompra() == null) {
			entidade.setDataCompra(new Date());
		}else {
			entidade.setDataCompra(dto.getDataCompra());
		}
		
		return entidade;
	}

}
