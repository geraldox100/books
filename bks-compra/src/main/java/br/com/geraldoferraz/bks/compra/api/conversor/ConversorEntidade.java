package br.com.geraldoferraz.bks.compra.api.conversor;

import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustNotBeNull;

import java.util.ArrayList;
import java.util.List;

import br.com.geraldoferraz.bks.compra.api.dto.LivroDTO;
import br.com.geraldoferraz.bks.entidades.LivroComprado;

public class ConversorEntidade {

	private List<LivroComprado> entidades = new ArrayList<>();
	private LivroComprado entidade;

	ConversorEntidade(LivroComprado entidade) {
		mustNotBeNull(entidade, "entidade");
		this.entidade = entidade;
	}

	ConversorEntidade(List<LivroComprado> entidades) {
		mustNotBeNull(entidades, "entidades");
		this.entidades = entidades;
	}

	public LivroDTO converter() {
		return converter(this.entidade);
	}

	public LivroDTO converter(LivroComprado entidade) {
		LivroDTO dto = new LivroDTO();

		dto.setId(entidade.getId());
		dto.setNome(entidade.getNome());
		dto.setAutor(entidade.getAutor().getNome());
		dto.setDataCompra(entidade.getDataCompra());
		dto.setPaginas(entidade.getPaginas());
		dto.setCategoria(entidade.getCategoria().getNome());

		return dto;
	}

	public List<LivroDTO> converterLista() {
		List<LivroDTO> retorno = new ArrayList<>();

		for (LivroComprado e : entidades) {
			retorno.add(converter(e));
		}

		return retorno;
	}

}
