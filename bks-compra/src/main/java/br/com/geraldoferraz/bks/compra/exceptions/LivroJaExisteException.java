package br.com.geraldoferraz.bks.compra.exceptions;

import java.net.URI;

import org.springframework.web.util.UriComponentsBuilder;

import br.com.geraldoferraz.bks.comum.exceptions.DadoDuplicadoException;
import br.com.geraldoferraz.bks.entidades.LivroComprado;

public class LivroJaExisteException extends DadoDuplicadoException {

	private static final long serialVersionUID = 1L;
	
	private String location;

	public LivroJaExisteException(String message) {
		super(message);
	}

	public LivroJaExisteException(LivroComprado livroComprado) {
		super("Livro " + livroComprado.getNome() + " encontrador para id " + livroComprado.getId());
		
		UriComponentsBuilder newInstance = UriComponentsBuilder.newInstance();
		
		URI uri = newInstance.path("/livros/{id}").buildAndExpand(livroComprado.getId()).toUri();
		location = uri.toString();
	}

	@Override
	public String getLocation() {
		return location;
	}

	public static void throwMe(LivroComprado l) {
		throw new LivroJaExisteException(l);
	}

}
