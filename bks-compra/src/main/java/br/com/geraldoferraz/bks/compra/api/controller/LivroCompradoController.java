package br.com.geraldoferraz.bks.compra.api.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.geraldoferraz.bks.compra.api.conversor.Conversor;
import br.com.geraldoferraz.bks.compra.api.dto.LivroDTO;
import br.com.geraldoferraz.bks.compra.service.LivroCompradoService;
import br.com.geraldoferraz.bks.entidades.LivroComprado;

@RestController
@RequestMapping("/livros")
public class LivroCompradoController {

	@Autowired
	private Conversor conversor;

	@Autowired
	private LivroCompradoService service;

	@GetMapping("/")
	public List<LivroDTO> list() {
		List<LivroComprado> livros = service.list();
		return conversor.entidades(livros).converterLista();
	}

	@PostMapping
	public ResponseEntity<LivroDTO> create(@RequestBody LivroDTO livro, UriComponentsBuilder uriBuilder) {
		LivroComprado livroComprado = conversor.dto(livro).converter();
		livroComprado = service.create(livroComprado);

		URI uri = uriBuilder.path("/livros/{id}").buildAndExpand(livroComprado.getId()).toUri();
		return ResponseEntity.created(uri).body(conversor.entidade(livroComprado).converter());
	}

	@GetMapping("/{id}")
	public ResponseEntity<LivroDTO> retrieve(@PathVariable Long id) {
		return ResponseEntity.ok(conversor.entidade(service.retrieve(id)).converter());
	}

	@PutMapping("/{id}")
	public ResponseEntity<LivroDTO> update(@PathVariable Long id, @RequestBody LivroDTO livro) {
		LivroComprado livroComprado = conversor.dto(livro).converter();
		livroComprado = service.update(id, livroComprado);
		return ResponseEntity.ok(conversor.entidade(livroComprado).converter());
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> delete(@PathVariable Long id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}

}
