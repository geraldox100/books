package br.com.geraldoferraz.bks.compra.service;

import static br.com.geraldoferraz.bks.comum.util.Assercoes.notNull;
import static br.com.geraldoferraz.bks.comum.util.Assercoes.notNullOrEmpty;
import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustBeGT;
import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustNotBeNull;
import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustNotBeNullOrEmpty;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.geraldoferraz.bks.compra.exceptions.LivroJaExisteException;
import br.com.geraldoferraz.bks.compra.exceptions.LivroNaoEncontradoException;
import br.com.geraldoferraz.bks.compra.repository.AutorRepository;
import br.com.geraldoferraz.bks.compra.repository.CategoriaRepository;
import br.com.geraldoferraz.bks.compra.repository.LivroCompradoRepository;
import br.com.geraldoferraz.bks.entidades.LivroComprado;

@Service
public class LivroCompradoService {

	@Autowired
	private LivroCompradoRepository livroCompradoRepository;

	@Autowired
	private AutorRepository autorRepository;
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	public List<LivroComprado> list() {
		return livroCompradoRepository.findAll();
	}

	public LivroComprado create(LivroComprado livroComprado) {
		validarCamposObrigatorios(livroComprado);
		validarQueLivroNaoExiste(livroComprado);
		
		autorRepository.findByNome(livroComprado.getAutor().getNome()).ifPresent(a -> {
			livroComprado.setAutor(a);
		});
		
		categoriaRepository.findByNome(livroComprado.getCategoria().getNome()).ifPresent(c -> {
			livroComprado.setCategoria(c);
		});
		
		return livroCompradoRepository.save(livroComprado);

	}

	public LivroComprado retrieve(Long id) {
		validarId(id);
		return livroCompradoRepository.findById(id).orElseThrow(() -> new LivroNaoEncontradoException(id));
	}

	public LivroComprado update(Long id, LivroComprado alteracao) {
		validarId(id);

		LivroComprado livroComprado = livroCompradoRepository.findById(id)
				.orElseThrow(() -> new LivroNaoEncontradoException(id));

		
		if(notNull(alteracao.getAutor()) && notNullOrEmpty(alteracao.getAutor().getNome())) {
			livroComprado.setAutor(alteracao.getAutor());
		}
		
		if(notNull(alteracao.getCategoria()) && notNullOrEmpty(alteracao.getCategoria().getNome())) {
			livroComprado.setCategoria(alteracao.getCategoria());
		}
		
		if(notNullOrEmpty(alteracao.getNome())) {
			livroComprado.setNome(alteracao.getNome());
		}
		
		if(notNull(alteracao.getPaginas())) {
			livroComprado.setPaginas(alteracao.getPaginas());
		}

		validarCamposObrigatorios(livroComprado);
		livroCompradoRepository.save(livroComprado);

		return livroComprado;
	}

	public void delete(Long id) {
		if (livroCompradoRepository.existsById(id)) {
			livroCompradoRepository.deleteById(id);
		} else {
			LivroNaoEncontradoException.throwMe(id);
		}
	}

	private void validarId(Long id) {
		mustNotBeNull(id, "id");
	}

	private void validarQueLivroNaoExiste(LivroComprado livroComprado) {
		Optional<LivroComprado> opt = livroCompradoRepository.findByNome(livroComprado.getNome());
		opt.ifPresent(l -> LivroJaExisteException.throwMe(l));
	}
	
	private void validarCamposObrigatorios(LivroComprado livroComprado) {
		mustNotBeNull(livroComprado,"livro");
		mustNotBeNull(livroComprado.getAutor(),"autor");
		mustNotBeNull(livroComprado.getCategoria(),"categoria");
		
		mustNotBeNullOrEmpty(livroComprado.getNome(), "nome livro");
		mustNotBeNullOrEmpty(livroComprado.getAutor().getNome(), "nome autor");
		mustNotBeNullOrEmpty(livroComprado.getCategoria().getNome(), "nome categoria");
		
		mustBeGT(livroComprado.getPaginas(),0,"paginas");
	}

}
