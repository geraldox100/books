package br.com.geraldoferraz.bks.compra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan("br.com.geraldoferraz.bks.entidades")
public class BksCompraApplication {
	
	private BksCompraApplication() {}

	private static SpringApplication app = new SpringApplication(BksCompraApplication.class);

	public static void main(String[] args) {
		app.run();
	}

}
