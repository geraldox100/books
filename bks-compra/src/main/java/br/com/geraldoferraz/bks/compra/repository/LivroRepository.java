package br.com.geraldoferraz.bks.compra.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.geraldoferraz.bks.entidades.Livro;

public interface LivroRepository extends JpaRepository<Livro, Long>{

}
