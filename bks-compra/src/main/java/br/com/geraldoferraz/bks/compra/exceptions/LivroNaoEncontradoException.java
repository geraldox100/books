package br.com.geraldoferraz.bks.compra.exceptions;

import br.com.geraldoferraz.bks.comum.exceptions.RecursoNaoEncontradoException;

public class LivroNaoEncontradoException extends RecursoNaoEncontradoException {

	private static final long serialVersionUID = 1L;

	public LivroNaoEncontradoException(String mensagem) {
		super(mensagem);
	}

	public LivroNaoEncontradoException(Long id) {
		super("Livro não encontrado para id: " + id);
	}
	
	public LivroNaoEncontradoException() {
		super("Livro não encontrado para id: ");
	}

	public static void throwMe(Long id) {
		throw new LivroNaoEncontradoException(id);
	}
}
