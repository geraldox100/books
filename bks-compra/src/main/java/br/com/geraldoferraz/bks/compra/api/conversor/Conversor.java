package br.com.geraldoferraz.bks.compra.api.conversor;

import java.util.List;

import org.springframework.stereotype.Component;

import br.com.geraldoferraz.bks.compra.api.dto.LivroDTO;
import br.com.geraldoferraz.bks.entidades.LivroComprado;

@Component
public class Conversor {

	public ConversorEntidade entidades(List<LivroComprado> entidades) {
		return new ConversorEntidade(entidades);
	}
	
	public ConversorEntidade entidade(LivroComprado entidade) {
		return new ConversorEntidade(entidade);
	}
	
	public ConversorDTO dto(LivroDTO dto) {
		return new ConversorDTO(dto);
	}

}