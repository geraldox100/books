package br.com.geraldoferraz.bks.compra.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.geraldoferraz.bks.entidades.LivroComprado;

public interface LivroCompradoRepository extends JpaRepository<LivroComprado,Long>{

	Optional<LivroComprado> findByNome(String nome);

}
