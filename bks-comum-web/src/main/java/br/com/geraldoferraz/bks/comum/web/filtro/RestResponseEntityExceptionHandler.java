package br.com.geraldoferraz.bks.comum.web.filtro;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.geraldoferraz.bks.comum.exceptions.DadoDuplicadoException;
import br.com.geraldoferraz.bks.comum.exceptions.RecursoNaoEncontradoException;


@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	// https://www.baeldung.com/exception-handling-for-rest-with-spring
	
	@ExceptionHandler(value = { RecursoNaoEncontradoException.class })
	protected ResponseEntity<Object> handleNotFound(RecursoNaoEncontradoException ex, WebRequest request) {
		return handleExceptionInternal(ex, ex.toJson(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
	
	@ExceptionHandler(value = { DadoDuplicadoException.class })
	protected ResponseEntity<Object> handleNotFound(DadoDuplicadoException ex, WebRequest request) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", ex.getLocation());
		return handleExceptionInternal(ex, ex.toJson(), headers, HttpStatus.CONFLICT, request);
	}
}