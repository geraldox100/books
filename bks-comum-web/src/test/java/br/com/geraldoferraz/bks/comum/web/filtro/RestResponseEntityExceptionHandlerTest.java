package br.com.geraldoferraz.bks.comum.web.filtro;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

import br.com.geraldoferraz.bks.comum.exceptions.DadoDuplicadoException;
import br.com.geraldoferraz.bks.comum.exceptions.RecursoNaoEncontradoException;

@ExtendWith(MockitoExtension.class)
public class RestResponseEntityExceptionHandlerTest {
	
	@Mock
	private WebRequest wr;
	
	@Spy
	private RestResponseEntityExceptionHandler r;
	
	
	@Nested
    class QuandoExecutarHandle {
		
		@Test
		public void recursoNaoEncontradoExceptionDeveProduzir404() {
			ResponseEntity<Object> re = r.handleNotFound(new RecursoNaoEncontradoException(), wr);
			assertEquals("{\"error\":\"RecursoNaoEncontradoException\",\"message\":\"Recurso não encontrado\"}", re.getBody());
			assertEquals("404 NOT_FOUND", re.getStatusCode().toString());
			assertEquals(404, re.getStatusCodeValue());
		}
		
		
		@Test
		public void dadosDuplicadosExceptionDeveProduzir409ComLocation() {
			ResponseEntity<Object> re = r.handleNotFound(getDDE(), wr);
			assertEquals("{\"error\":\"\",\"message\":\"mensagem\"}", re.getBody());
			assertEquals("[Location:\"location\"]", re.getHeaders().toString());
			assertEquals("409 CONFLICT", re.getStatusCode().toString());
			assertEquals(409, re.getStatusCodeValue());
		}
		
		
	}
	

	private DadoDuplicadoException getDDE() {
		return new DadoDuplicadoException("mensagem") {
			
			private static final long serialVersionUID = 1L;

			@Override
			public String getLocation() {
				return "location";
			}
		};
	}

}