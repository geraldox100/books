package br.com.geraldoferraz.bks.meta.api.dto;

import java.io.Serializable;
import java.util.Date;

public class MetaDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private String titulo;

	private Date dataInicio;
	private Date dataFim;

	private Integer quantidade;
	
	private String tipo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipoMeta) {
		this.tipo = tipoMeta;
	}

}
