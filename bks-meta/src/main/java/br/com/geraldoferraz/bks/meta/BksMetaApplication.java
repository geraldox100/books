package br.com.geraldoferraz.bks.meta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan("br.com.geraldoferraz.bks.entidades")
public class BksMetaApplication {
	private BksMetaApplication() {}
	
	private static SpringApplication app = new SpringApplication(BksMetaApplication.class);

	public static void main(String[] args) {
		app.run();
	}

}
