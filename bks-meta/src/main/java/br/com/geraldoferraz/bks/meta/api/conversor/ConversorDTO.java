package br.com.geraldoferraz.bks.meta.api.conversor;

import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustNotBeNull;

import br.com.geraldoferraz.bks.entidades.Meta;
import br.com.geraldoferraz.bks.entidades.TipoMeta;
import br.com.geraldoferraz.bks.meta.api.dto.MetaDTO;

public class ConversorDTO {

	private MetaDTO dto;

	ConversorDTO(MetaDTO dto) {
		mustNotBeNull(dto,"MetaDTO");
		this.dto = dto;
	}

	public Meta converter() {
		Meta retorno = new Meta();
		
		retorno.setId(dto.getId());
		retorno.setTitulo(dto.getTitulo());
		retorno.setDataInicio(dto.getDataInicio());
		retorno.setDataFim(dto.getDataFim());
		retorno.setQuantidade(dto.getQuantidade());
		retorno.setTipo(TipoMeta.valueOf(dto.getTipo()));
		
		return retorno;
	}


}
