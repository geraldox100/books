package br.com.geraldoferraz.bks.meta.api.conversor;

import java.util.List;

import org.springframework.stereotype.Component;

import br.com.geraldoferraz.bks.entidades.Meta;
import br.com.geraldoferraz.bks.meta.api.dto.MetaDTO;

@Component
public class Conversor {

	public ConversorEntidade entidades(List<Meta> entidades) {
		return new ConversorEntidade(entidades);
	}
	
	public ConversorEntidade entidade(Meta entidade) {
		return new ConversorEntidade(entidade);
	}
	
	public ConversorDTO dto(MetaDTO dto) {
		return new ConversorDTO(dto);
	}

}
