package br.com.geraldoferraz.bks.meta.service;

import static br.com.geraldoferraz.bks.comum.util.Assercoes.notNull;
import static br.com.geraldoferraz.bks.comum.util.Assercoes.notNullOrEmpty;
import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustNotBeNull;
import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustNotBeNullOrEmpty;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.geraldoferraz.bks.entidades.Meta;
import br.com.geraldoferraz.bks.meta.exceptions.MetaNaoEncontradoException;
import br.com.geraldoferraz.bks.meta.repository.MetaRepository;

@Service
public class MetaService {

	@Autowired
	private MetaRepository repo;

	public List<Meta> list() {
		return repo.findAll();
	}

	public Meta create(Meta meta) {
		validarCamposObrigatorios(meta);
		return repo.save(meta);
	}

	public Meta retrieve(Long id) {
		validarId(id);
		return repo.findById(id).orElseThrow(() -> new MetaNaoEncontradoException(id));
	}

	public Meta update(Long id, Meta alteracao) {
		validarId(id);
		validarMeta(alteracao);

		Meta meta = repo.findById(id).orElseThrow(() -> new MetaNaoEncontradoException(id));
		
		if(notNull(alteracao.getTipo())) {
			meta.setTipo(alteracao.getTipo());
		}
		
		if(notNullOrEmpty(alteracao.getTitulo())) {
			meta.setTitulo(alteracao.getTitulo());
		}
		
		if(notNull(alteracao.getDataInicio())) {
			meta.setDataInicio(alteracao.getDataInicio());
		}
		
		if(notNull(alteracao.getDataFim())) {
			meta.setDataFim(alteracao.getDataFim());
		}
		
		if(notNull(alteracao.getQuantidade())) {
			meta.setQuantidade(alteracao.getQuantidade());
		}

		
		validarCamposObrigatorios(meta);

		return repo.save(meta);
	}

	public void delete(Long id) {
		if (repo.existsById(id)) {
			repo.deleteById(id);
		} else {
			throw new MetaNaoEncontradoException(id);
		}
	}

	private void validarId(Long id) {
		mustNotBeNull(id, "id");
	}

	private void validarMeta(Meta meta) {
		mustNotBeNull(meta, "meta");
	}
	
	private void validarCamposObrigatorios(Meta meta) {
		mustNotBeNull(meta, "meta");
		mustNotBeNull(meta.getDataInicio(), "data inicio");
		mustNotBeNull(meta.getDataFim(), "data fim");
		mustNotBeNull(meta.getQuantidade(), "quantidade");
		mustNotBeNull(meta.getTipo(), "tipo");
		mustNotBeNullOrEmpty(meta.getTitulo(), "titulo");
	}

}
