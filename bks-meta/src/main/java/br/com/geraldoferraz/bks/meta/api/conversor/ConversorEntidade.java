package br.com.geraldoferraz.bks.meta.api.conversor;

import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustNotBeNull;

import java.util.ArrayList;
import java.util.List;

import br.com.geraldoferraz.bks.entidades.Meta;
import br.com.geraldoferraz.bks.meta.api.dto.MetaDTO;

public class ConversorEntidade extends Conversor {

	private List<Meta> entidades = new ArrayList<>();
	private Meta entidade;
	
	ConversorEntidade(Meta entidade) {
		mustNotBeNull(entidade, "entidade");
		this.entidade = entidade;
	}

	ConversorEntidade(List<Meta> entidades) {
		mustNotBeNull(entidades, "entidades");
		this.entidades = entidades;
	}

	public List<MetaDTO> converterLista() {
		List<MetaDTO> retorno = new ArrayList<>();
		for (Meta meta : entidades) {
			retorno.add(converter(meta));
		}
		return retorno;
	}
	
	public MetaDTO converter() {
		return converter(entidade);
	}

	private MetaDTO converter(Meta e) {
		MetaDTO dto = new MetaDTO();
		dto.setId(e.getId());
		
		dto.setTitulo(e.getTitulo());

		dto.setDataInicio(e.getDataInicio());
		dto.setDataFim(e.getDataFim());

		dto.setQuantidade(e.getQuantidade());
		
		dto.setTipo(e.getTipo().getDescricao());
		
		return dto;
	}

	

}
