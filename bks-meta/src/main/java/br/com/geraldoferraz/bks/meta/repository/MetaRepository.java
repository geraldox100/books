package br.com.geraldoferraz.bks.meta.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.geraldoferraz.bks.entidades.Meta;

public interface MetaRepository extends JpaRepository<Meta, Long>{

}
