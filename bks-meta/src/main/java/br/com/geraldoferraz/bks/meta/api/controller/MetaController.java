package br.com.geraldoferraz.bks.meta.api.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.geraldoferraz.bks.entidades.Meta;
import br.com.geraldoferraz.bks.meta.api.conversor.Conversor;
import br.com.geraldoferraz.bks.meta.api.dto.MetaDTO;
import br.com.geraldoferraz.bks.meta.service.MetaService;

@RestController
@RequestMapping("/meta")
public class MetaController {

	@Autowired
	private MetaService service;
	
	@Autowired
	private Conversor conversor;

	@GetMapping
	public List<MetaDTO> list() {
		return conversor.entidades(service.list()).converterLista(); 
	}

	@PostMapping
	public ResponseEntity<MetaDTO> create(@RequestBody MetaDTO dto, UriComponentsBuilder uriBuilder) {
		Meta meta = conversor.dto(dto).converter();
		meta = service.create(meta);

		URI uri = uriBuilder.path("/meta/{id}").buildAndExpand(meta.getId()).toUri();
		return ResponseEntity.created(uri).body(conversor.entidade(meta).converter());
	}

	@GetMapping("/{id}")
	public ResponseEntity<MetaDTO> retrieve(@PathVariable Long id) {
		return ResponseEntity.ok(conversor.entidade(service.retrieve(id)).converter());
	}

	@PutMapping("/{id}")
	public ResponseEntity<MetaDTO> update(@PathVariable Long id, @RequestBody MetaDTO dto) {
		Meta meta = conversor.dto(dto).converter();
		return ResponseEntity.ok(conversor.entidade(service.update(id, meta)).converter());
	}

	@PostMapping("/{id}")
	public ResponseEntity<Object> delete(@PathVariable Long id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}

}
