package br.com.geraldoferraz.bks.meta.exceptions;

import br.com.geraldoferraz.bks.comum.exceptions.RecursoNaoEncontradoException;

public class MetaNaoEncontradoException extends RecursoNaoEncontradoException {

	public static final String MENSAGEM_PADRAO = "Meta não encontrado";
	private static final long serialVersionUID = 1L;
	
	public MetaNaoEncontradoException(String mensagem) {
		super(mensagem);
	}

	public MetaNaoEncontradoException(Long id) {
		super("Meta não encontrado para id: " + id);
	}
	
	public MetaNaoEncontradoException() {
		super(MENSAGEM_PADRAO);
	}

	public static void throwMe(Long id) {
		throw new MetaNaoEncontradoException(id);
	}

}
