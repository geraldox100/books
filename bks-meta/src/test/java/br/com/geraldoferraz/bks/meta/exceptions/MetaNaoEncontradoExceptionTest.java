package br.com.geraldoferraz.bks.meta.exceptions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class MetaNaoEncontradoExceptionTest {

	@Nested
	class QuandoConstruirMetaNaoEncontradoException {

		@Test
		public void comConstrutorPadraoDeveConstruirMensagemPadrao() {
			MetaNaoEncontradoException e = new MetaNaoEncontradoException();
			assertEquals(MetaNaoEncontradoException.MENSAGEM_PADRAO, e.getMessage());
		}

		@Test
		public void passandoMensagemDeveConstruirMensagemComTextoParametrizada() {
			String mensagem = "mensagem parametrizada";
			MetaNaoEncontradoException e = new MetaNaoEncontradoException(mensagem);
			assertEquals(mensagem, e.getMessage());
		}

		@Test
		public void passandoIdDeveConstruirMensagemComIdParametrizado() {
			MetaNaoEncontradoException e = new MetaNaoEncontradoException(2l);
			assertEquals("Meta não encontrado para id: 2", e.getMessage());
		}

	}

	@Nested
	class QuandoExecutarThrowMe {
		@Test
		public void deveLancarExcecao() {
			Assertions.assertThrows(MetaNaoEncontradoException.class, () -> MetaNaoEncontradoException.throwMe(3l));
		}
	}

}
