package br.com.geraldoferraz.bks.meta.api.conversor;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import br.com.geraldoferraz.bks.entidades.Meta;
import br.com.geraldoferraz.bks.entidades.TipoMeta;
import br.com.geraldoferraz.bks.meta.api.dto.MetaDTO;

public class ConversorEntidadeTest {

	private ConversorEntidade conversor;

	@Nested
	class QuandoConstruir {

		@Test
		public void passandoEntidadeNula() {
			Meta meta = null;
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
					() -> new ConversorEntidade(meta));
			assertEquals("Campo entidade não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoListaNula() {
			List<Meta> metas = null;
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
					() -> new ConversorEntidade(metas));
			assertEquals("Campo entidades não pode ser nulo", e.getMessage());
		}
	}
	
	@Nested
	class QuandoConverterLista{
		@Test
		public void quandoConverterListaComListaVazia() {
			conversor = new ConversorEntidade(new ArrayList<>());
			
			List<MetaDTO> lista = conversor.converterLista();
			assertNotNull(lista);
			assertEquals(0, lista.size());
			
		}
		
		@Test
		public void quandoConverterListaComUmElemento() {
			ArrayList<Meta> entidades = new ArrayList<>();
			entidades.add(criarMeta());
			
			Date dataInicio = new Date();
			Date dataFim = new Date();
			for (Meta meta : entidades) {
				meta.setDataInicio(dataInicio);
				meta.setDataFim(dataFim);
			}
			
			conversor = new ConversorEntidade(entidades);
			
			List<MetaDTO> lista = conversor.converterLista();
			assertEquals(1, lista.size());
			
			MetaDTO dto = lista.get(0);
			
			assertAll("metaDTO", 
					() -> assertEquals(1l, dto.getId()),
					() -> assertEquals("minha meta", dto.getTitulo()),
					() -> assertEquals(100, dto.getQuantidade()),
					() -> assertEquals(TipoMeta.ANUAL.getDescricao(), dto.getTipo()),
					() -> assertEquals(dataInicio, dto.getDataInicio()), 
					() -> assertEquals(dataFim, dto.getDataFim()));
			
		}
		
		@Test
		public void quandoConverterListaVariosElementos() {
			ArrayList<Meta> entidades = new ArrayList<>();

			entidades.add(criarMeta());
			entidades.add(criarMeta());
			entidades.add(criarMeta());
			
			Date dataInicio = new Date();
			Date dataFim = new Date();
			for (Meta meta : entidades) {
				meta.setDataInicio(dataInicio);
				meta.setDataFim(dataFim);
			}
			
			conversor = new ConversorEntidade(entidades);
			
			List<MetaDTO> lista = conversor.converterLista();
			assertEquals(3, lista.size());
			
			for (MetaDTO dto : lista) {
				assertAll("metaDTO", 
						() -> assertEquals(1l, dto.getId()),
						() -> assertEquals("minha meta", dto.getTitulo()),
						() -> assertEquals(100, dto.getQuantidade()),
						() -> assertEquals(TipoMeta.ANUAL.getDescricao(), dto.getTipo()),
						() -> assertEquals(dataInicio, dto.getDataInicio()), 
						() -> assertEquals(dataFim, dto.getDataFim()));
			}
			
		}
	}
	
	@Nested
	class QuandoConverterEntidade{
		@Test
		public void passandoUmElementoDeveConstruirListaComUmElemento() {
			Meta meta = criarMeta();
			
			Date dataInicio = new Date();
			Date dataFim = new Date();
			meta.setDataInicio(dataInicio);
			meta.setDataFim(dataFim);
			
			conversor = new ConversorEntidade(meta);
			
			MetaDTO dto = conversor.converter();
			
			assertAll("metaDTO", 
					() -> assertEquals(1l, dto.getId()),
					() -> assertEquals("minha meta", dto.getTitulo()),
					() -> assertEquals(100, dto.getQuantidade()),
					() -> assertEquals(TipoMeta.ANUAL.getDescricao(), dto.getTipo()),
					() -> assertEquals(dataInicio, dto.getDataInicio()), 
					() -> assertEquals(dataFim, dto.getDataFim()));
			
		}
	}
	
	private Meta criarMeta() {
		Meta meta = new Meta();
		meta.setId(1l);
		meta.setTitulo("minha meta");
		meta.setQuantidade(100);
		meta.setDataInicio(new Date());
		meta.setDataFim(new Date());
		meta.setTipo(TipoMeta.ANUAL);
		return meta;
	}


}
