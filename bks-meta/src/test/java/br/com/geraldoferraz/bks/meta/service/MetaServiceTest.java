package br.com.geraldoferraz.bks.meta.service;

import static org.junit.Assert.assertSame;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.geraldoferraz.bks.entidades.Meta;
import br.com.geraldoferraz.bks.entidades.TipoMeta;
import br.com.geraldoferraz.bks.meta.exceptions.MetaNaoEncontradoException;
import br.com.geraldoferraz.bks.meta.repository.MetaRepository;

@ExtendWith(MockitoExtension.class)
public class MetaServiceTest {

	@Mock
	private MetaRepository repo;

	@InjectMocks
	private MetaService service;

	@Nested
	class QuandoExecutarLista {
		@Test
		public void deveRetornarLista() {
			service.list();
			verify(repo, times(1)).findAll();
		}
	}

	@Nested
	class DeveLancarExcecaoQuandoExecutarCreate {
		@Test
		public void passandoMetaNulaDeveLancarExcecao() {
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.create(null));
			assertEquals("Campo meta não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoDataInicioNulaDeveLancarExcecao() {
			Meta meta = criarMeta();
			meta.setDataInicio(null);
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.create(meta));
			assertEquals("Campo data inicio não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoDataFimNulaDeveLancarExcecao() {
			Meta meta = criarMeta();
			meta.setDataFim(null);
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.create(meta));
			assertEquals("Campo data fim não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoQuantidadeNulaDeveLancarExcecao() {
			Meta meta = criarMeta();
			meta.setQuantidade(null);
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.create(meta));
			assertEquals("Campo quantidade não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoTipoNulaDeveLancarExcecao() {
			Meta meta = criarMeta();
			meta.setTipo(null);
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.create(meta));
			assertEquals("Campo tipo não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoTituloNulaDeveLancarExcecao() {
			Meta meta = criarMeta();
			meta.setTitulo(null);
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.create(meta));
			assertEquals("Campo titulo não pode ser nulo ou vazio", e.getMessage());
		}

		@Test
		public void passandoTituloVazioDeveLancarExcecao() {
			Meta meta = criarMeta();
			meta.setTitulo("");
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.create(meta));
			assertEquals("Campo titulo não pode ser nulo ou vazio", e.getMessage());
		}

		@Test
		public void passandoMetaCompletaDevePersistirChamandoRepo() {
			Meta meta = criarMeta();
			service.create(meta);
			verify(repo, times(1)).save(meta);
		}

	}

	@Nested
	class QuandoExecutarRetrieve {
		@Test
		public void passandoIdNullDeveLancarExcecao() {
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.retrieve(null));
			assertEquals("Campo id não pode ser nulo", e.getMessage());
		}

		@Test
		public void passandoIdQueNaoExisteDeveLancarExcecao() {
			when(repo.findById(1l)).thenReturn(java.util.Optional.empty());
			assertThrows(MetaNaoEncontradoException.class, () -> service.retrieve(1l));
		}

		@Test
		public void passandoIdValidoDeveRetornarObjetoEncontrado() {
			Meta meta = criarMeta();
			when(repo.findById(1l)).thenReturn(java.util.Optional.of(meta));
			Meta metaEncontrada = service.retrieve(1l);
			assertSame(meta, metaEncontrada);
		}
	}
	
	@Nested
	class QuandoExecutarUpdate{
		@Test
		public void passandoIdNullDeveLancarExcecao() {
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.update(null, criarMeta()));
			assertEquals("Campo id não pode ser nulo", e.getMessage());
		}
		
		@Test
		public void passandoMetaNullDeveLancarExcecao() {
			IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> service.update(2l, null));
			assertEquals("Campo meta não pode ser nulo", e.getMessage());
		}
		
		@Test
		public void passandoIdQueNaoExisteDeveLancarExcecao() {
			when(repo.findById(2l)).thenReturn(Optional.empty());
			MetaNaoEncontradoException e = assertThrows(MetaNaoEncontradoException.class, () -> service.update(2l, criarMeta()));
			assertEquals("Meta não encontrado para id: 2", e.getMessage());
		}
		
		@Test
		public void quandoPassarAlteracaoDeApenasUmCampoValidaDeveSalvar() {
			Meta meta = criarMeta();
			when(repo.findById(2l)).thenReturn(java.util.Optional.of(meta));
			when(repo.save(meta)).thenReturn(meta);
			
			Meta alteracao = new Meta();
			alteracao.setTitulo("sua alteracao");
			
			Meta produto = service.update(2l, alteracao);
			
			verify(repo, times(1)).save(meta);
			
			assertSame(meta, produto);
			assertEquals(alteracao.getTitulo(), produto.getTitulo());
			assertEquals(meta.getId(), produto.getId());
			assertEquals(meta.getDataInicio(), produto.getDataInicio());
			assertEquals(meta.getDataFim(), produto.getDataFim());
			assertEquals(meta.getQuantidade(), produto.getQuantidade());
			assertEquals(meta.getTipo(), produto.getTipo());
		}
		
		@Test
		public void quandoPassarAlteracaoDeTodosOsCampoValidaDeveSalvar() {
			Meta meta = criarMeta();
			when(repo.findById(2l)).thenReturn(java.util.Optional.of(meta));
			when(repo.save(meta)).thenReturn(meta);
			
			Meta alteracao = new Meta();
			alteracao.setTitulo("sua alteracao");
			alteracao.setDataInicio(new Date());
			alteracao.setDataFim(new Date());
			alteracao.setQuantidade(33);
			alteracao.setTipo(TipoMeta.NAO_USAR);
			
			Meta produto = service.update(2l, alteracao);
			
			verify(repo, times(1)).save(meta);
			
			assertSame(meta, produto);
			assertEquals(alteracao.getTitulo(), produto.getTitulo());
			assertEquals(alteracao.getId(), produto.getId());
			assertEquals(alteracao.getDataInicio(), produto.getDataInicio());
			assertEquals(alteracao.getDataFim(), produto.getDataFim());
			assertEquals(alteracao.getQuantidade(), produto.getQuantidade());
			assertEquals(alteracao.getTipo(), produto.getTipo());
		}
		
		@Test
		public void quandoPassarApenasTituloVazioNaoDeveAlterar() {
			Meta meta = criarMeta();
			when(repo.findById(2l)).thenReturn(java.util.Optional.of(meta));
			when(repo.save(meta)).thenReturn(meta);
			
			Meta alteracao = new Meta();
			alteracao.setTitulo("");
			Meta produto = service.update(2l, alteracao);
			
			assertSame(meta, produto);
			assertEquals(meta.getTitulo(), produto.getTitulo());
		}
		
		@Test
		public void quandoPassarApenasTituloNuloNaoDeveAlterar() {
			Meta meta = criarMeta();
			when(repo.findById(2l)).thenReturn(java.util.Optional.of(meta));
			when(repo.save(meta)).thenReturn(meta);
			
			Meta alteracao = new Meta();
			alteracao.setTitulo(null);
			Meta produto = service.update(2l, alteracao);
			
			assertSame(meta, produto);
			assertEquals(meta.getTitulo(), produto.getTitulo());
		}
		
		@Test
		public void quandoPassarApenasTipoNuloNaoDeveAlterar() {
			Meta meta = criarMeta();
			when(repo.findById(2l)).thenReturn(java.util.Optional.of(meta));
			when(repo.save(meta)).thenReturn(meta);
			
			Meta alteracao = new Meta();
			alteracao.setTipo(null);
			Meta produto = service.update(2l, alteracao);
			
			assertSame(meta, produto);
			assertEquals(meta.getTipo(), produto.getTipo());
		}
		
	}
	
	@Nested
	class QuandoExecutarDelete{
		@Test
		public void quandoPassarIdQueNaoExisteDeveLancarExcecao() {
			when(repo.existsById(2l)).thenReturn(false);
			Mockito.verify(repo,never()).deleteById(1l);
			MetaNaoEncontradoException e = assertThrows(MetaNaoEncontradoException.class, () -> service.delete(2l));
			assertEquals("Meta não encontrado para id: 2", e.getMessage());
		}
		@Test
		public void quandoPassarIdQueExisteDeveDeletar() {
			when(repo.existsById(1l)).thenReturn(true);
			service.delete(1l);
			Mockito.verify(repo,times(1)).deleteById(1l);
		}
	}
	
	

	private Meta criarMeta() {
		Meta meta = new Meta();
		meta.setDataInicio(new Date());
		meta.setDataFim(new Date());
		meta.setQuantidade(100);
		meta.setTipo(TipoMeta.ANUAL);
		meta.setTitulo("minha meta");
		return meta;
	}

}
