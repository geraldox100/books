package br.com.geraldoferraz.bks.meta.api.conversor;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.com.geraldoferraz.bks.entidades.Meta;
import br.com.geraldoferraz.bks.meta.api.dto.MetaDTO;

public class ConversorTest {
	
	private Conversor conversor;
	
	@BeforeEach
	public void beforeEach() {
		conversor = new Conversor();
	}
	
	@Test
	public void quandoExecutarDTODeveRetornarInstanciaDeConversorDTO() {
		ConversorDTO dto = conversor.dto(new MetaDTO());
		assertNotNull(dto);
	}
	
	@Test
	public void quandoExecutarEntidadeDeveRetornarInstanciaDeConversorEntidade() {
		ConversorEntidade entidade = conversor.entidade(new Meta());
		assertNotNull(entidade);
	}
	
	@Test
	public void quandoExecutarEntidadesDeveRetornarInstanciaDeConversorEntidade() {
		ConversorEntidade entidade = conversor.entidades(asList(new Meta()));
		assertNotNull(entidade);
	}

}
