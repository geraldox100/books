package br.com.geraldoferraz.bks.meta.api.dto;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import org.junit.jupiter.api.Test;

import br.com.geraldoferraz.bks.entidades.TipoMeta;

public class MetaDTOTest {

	@Test
	public void quandoExecutarSettersDevePreencherDTO() {
		MetaDTO meta = criarMeta();
		Date dataInicio = new Date();
		Date dataFim = new Date();
		meta.setDataInicio(dataInicio);
		meta.setDataFim(dataFim);
		assertAll("meta", 
				() -> assertEquals(1l, meta.getId()),
				() -> assertEquals("minha meta", meta.getTitulo()),
				() -> assertEquals(100, meta.getQuantidade()),
				() -> assertEquals(TipoMeta.ANUAL.getDescricao(), meta.getTipo()),
				() -> assertEquals(dataInicio, meta.getDataInicio()), 
				() -> assertEquals(dataFim, meta.getDataFim()));
	}

	private MetaDTO criarMeta() {
		MetaDTO meta = new MetaDTO();
		meta.setId(1l);
		meta.setTitulo("minha meta");
		meta.setQuantidade(100);
		meta.setDataInicio(new Date());
		meta.setDataFim(new Date());
		meta.setTipo(TipoMeta.ANUAL.getDescricao());
		return meta;
	}

}
