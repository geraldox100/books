package br.com.geraldoferraz.bks.meta.api.controller;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.geraldoferraz.bks.entidades.Meta;
import br.com.geraldoferraz.bks.entidades.TipoMeta;
import br.com.geraldoferraz.bks.meta.api.conversor.Conversor;
import br.com.geraldoferraz.bks.meta.api.conversor.ConversorDTO;
import br.com.geraldoferraz.bks.meta.api.conversor.ConversorEntidade;
import br.com.geraldoferraz.bks.meta.api.dto.MetaDTO;
import br.com.geraldoferraz.bks.meta.service.MetaService;

@ExtendWith(MockitoExtension.class)
public class MetaControllerTest {

	@Mock
	private MetaService service;

	@Mock
	private Conversor conversor;

	@Mock
	private ConversorEntidade conversorEntidade;

	@Mock
	private ConversorDTO conversorDTO;

	@Spy
	private UriComponentsBuilder uriBuilder;

	@InjectMocks
	private MetaController controller;

	@BeforeEach
	public void beforeEach() {
		lenient().when(conversor.entidade(Mockito.any())).thenReturn(conversorEntidade);
		lenient().when(conversor.entidades(Mockito.anyList())).thenReturn(conversorEntidade);
		lenient().when(conversor.dto(Mockito.any(MetaDTO.class))).thenReturn(conversorDTO);
	}

	@Test
	public void quandoExecutarList() {
		controller.list();
		verify(service, times(1)).list();
		verify(conversorEntidade, times(1)).converterLista();
	}

	@Test
	public void quandoExecutarCreate() {
		MetaDTO dto = criarDTO();
		Meta entidade = criarEntidade();

		when(conversorDTO.converter()).thenReturn(entidade);
		when(service.create(Mockito.any())).thenReturn(entidade);
		when(conversorEntidade.converter()).thenReturn(dto);

		ResponseEntity<MetaDTO> response = controller.create(dto, uriBuilder);

		verify(service, times(1)).create(entidade);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertEquals(dto, response.getBody());
	}

	@Test
	public void quandoExecutarRetrieve() {
		MetaDTO dto = criarDTO();
		when(conversorEntidade.converter()).thenReturn(dto);

		ResponseEntity<MetaDTO> response = controller.retrieve(1l);

		verify(service, times(1)).retrieve(1l);
		verify(conversorEntidade, times(1)).converter();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(dto, response.getBody());
	}
	
	@Test
	public void quandoExecutarUpdate() {
		MetaDTO dto = criarDTO();
		Meta entidade = criarEntidade();

		when(conversorDTO.converter()).thenReturn(entidade);
		when(conversorEntidade.converter()).thenReturn(dto);

		ResponseEntity<MetaDTO> response = controller.update(1l, dto);

		verify(service, times(1)).update(1l, entidade);
		verify(conversorEntidade, times(1)).converter();
		verify(conversorDTO, times(1)).converter();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(dto, response.getBody());
	}
	
	@Test
	public void quandoExecutarDelete() {
		ResponseEntity<Object> response = controller.delete(1l);
		verify(service, times(1)).delete(1l);
		
		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
		assertNull(response.getBody());
	}

	private MetaDTO criarDTO() {
		MetaDTO meta = new MetaDTO();
		meta.setId(1l);
		meta.setTitulo("minha meta");
		meta.setQuantidade(100);
		meta.setDataInicio(new Date());
		meta.setDataFim(new Date());
		meta.setTipo(TipoMeta.ANUAL.getDescricao());
		return meta;
	}

	private Meta criarEntidade() {
		Meta meta = new Meta();
		meta.setId(1l);
		meta.setTitulo("minha meta");
		meta.setQuantidade(100);
		meta.setDataInicio(new Date());
		meta.setDataFim(new Date());
		meta.setTipo(TipoMeta.ANUAL);
		return meta;
	}

}
