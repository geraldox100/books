package br.com.geraldoferraz.bks.meta.api.conversor;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Date;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import br.com.geraldoferraz.bks.entidades.Meta;
import br.com.geraldoferraz.bks.entidades.TipoMeta;
import br.com.geraldoferraz.bks.meta.api.dto.MetaDTO;

public class ConversorDTOTest {
	

	@Test
	public void quandoConstruirPassandoEntidadeNula() {
		MetaDTO meta = null;
		IllegalArgumentException e = assertThrows(IllegalArgumentException.class, () -> new ConversorDTO(meta));
		assertEquals("Campo MetaDTO não pode ser nulo", e.getMessage());

	}
	
	@Nested
	class QuandoConverterDTO{
		@Test
		public void quandoConverterListaComUmElemento() {
			MetaDTO meta = criarMeta();
			
			Date dataInicio = new Date();
			Date dataFim = new Date();
			meta.setDataInicio(dataInicio);
			meta.setDataFim(dataFim);
			
			ConversorDTO conversor = new ConversorDTO(meta);
			
			Meta dto = conversor.converter();
			
			assertAll("meta", 
					() -> assertEquals(1l, dto.getId()),
					() -> assertEquals("minha meta", dto.getTitulo()),
					() -> assertEquals(100, dto.getQuantidade()),
					() -> assertEquals(TipoMeta.ANUAL, dto.getTipo()),
					() -> assertEquals(dataInicio, dto.getDataInicio()), 
					() -> assertEquals(dataFim, dto.getDataFim()));
			
		}
	}
	
	private MetaDTO criarMeta() {
		MetaDTO meta = new MetaDTO();
		meta.setId(1l);
		meta.setTitulo("minha meta");
		meta.setQuantidade(100);
		meta.setDataInicio(new Date());
		meta.setDataFim(new Date());
		meta.setTipo(TipoMeta.ANUAL.getDescricao());
		return meta;
	}

}
