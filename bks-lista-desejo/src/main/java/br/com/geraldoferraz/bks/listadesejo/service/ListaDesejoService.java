package br.com.geraldoferraz.bks.listadesejo.service;

import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustNotBeNull;
import static br.com.geraldoferraz.bks.comum.util.Validacoes.mustNotBeNullOrEmpty;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.geraldoferraz.bks.entidades.ListaDesejo;
import br.com.geraldoferraz.bks.listadesejo.exceptions.ListaDesejoJaExisteException;
import br.com.geraldoferraz.bks.listadesejo.exceptions.ListaDesejoNaoEncontradoException;
import br.com.geraldoferraz.bks.listadesejo.repository.ListaDesejoRepository;

@Service
public class ListaDesejoService {

	@Autowired
	private ListaDesejoRepository listaDesejoRepository;

	public List<ListaDesejo> list() {
		return listaDesejoRepository.findAll();
	}

	public ListaDesejo create(ListaDesejo lista) {
		validarcamposObrigatorios(lista);
		validarQueListaNaoExiste(lista);
		return listaDesejoRepository.save(lista);
	}

	public ListaDesejo retrieve(Long id) {
		validarId(id);
		return listaDesejoRepository.findById(id).orElseThrow(() -> new ListaDesejoNaoEncontradoException(id));
	}

	public ListaDesejo update(Long id, ListaDesejo alteracao) {
		validarId(id);

		ListaDesejo lista = listaDesejoRepository.findById(id)
				.orElseThrow(() -> new ListaDesejoNaoEncontradoException(id));

		lista.setNome(alteracao.getNome());
		validarcamposObrigatorios(lista);
		listaDesejoRepository.save(lista);

		return lista;
	}

	public void delete(Long id) {
		if (listaDesejoRepository.existsById(id)) {
			listaDesejoRepository.deleteById(id);
		} else {
			ListaDesejoNaoEncontradoException.throwMe(id);
		}
	}

	private void validarId(Long id) {
		mustNotBeNull(id, "id");
	}

	private void validarcamposObrigatorios(ListaDesejo lista) {
		mustNotBeNull(lista, "listaDesejo");
		mustNotBeNullOrEmpty(lista.getNome(), "nome");
	}

	private void validarQueListaNaoExiste(ListaDesejo lista) {
		Optional<ListaDesejo> opt = listaDesejoRepository.findByNome(lista.getNome());
		opt.ifPresent(l -> ListaDesejoJaExisteException.throwMe(l));
	}

}
