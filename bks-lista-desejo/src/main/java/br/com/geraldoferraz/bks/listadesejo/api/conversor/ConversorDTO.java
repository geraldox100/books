package br.com.geraldoferraz.bks.listadesejo.api.conversor;

import java.util.Date;

import br.com.geraldoferraz.bks.entidades.ListaDesejo;
import br.com.geraldoferraz.bks.listadesejo.api.dto.ListaDesejoDTO;

public class ConversorDTO {

	private ListaDesejoDTO dto;

	public ConversorDTO(ListaDesejoDTO dto) {
		this.dto = dto;
	}

	public ListaDesejo converter() {
		ListaDesejo entidade = new ListaDesejo();

		entidade.setNome(dto.getNome());
		
		if (dto.getDataCriacao() == null) {
			entidade.setDataCriacao(new Date());
		} else {
			entidade.setDataCriacao(dto.getDataCriacao());
		}

		return entidade;
	}

}
