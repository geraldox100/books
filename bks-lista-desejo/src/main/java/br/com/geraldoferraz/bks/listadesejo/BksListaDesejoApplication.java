package br.com.geraldoferraz.bks.listadesejo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan("br.com.geraldoferraz.bks.entidades")
public class BksListaDesejoApplication {
	
	private static SpringApplication app = new SpringApplication(BksListaDesejoApplication.class);

	public static void main(String[] args) {
		app.run();
	}

}
