package br.com.geraldoferraz.bks.listadesejo.exceptions;

import br.com.geraldoferraz.bks.comum.exceptions.RecursoNaoEncontradoException;

public class ListaDesejoNaoEncontradoException extends RecursoNaoEncontradoException {

	private static final long serialVersionUID = 1L;

	public ListaDesejoNaoEncontradoException(String mensagem) {
		super(mensagem);
	}

	public ListaDesejoNaoEncontradoException(Long id) {
		super("Lista não encontrado para id: " + id);
	}
	
	public ListaDesejoNaoEncontradoException() {
		super("Lista não encontrado para id: ");
	}

	public static void throwMe(Long id) {
		throw new ListaDesejoNaoEncontradoException(id);
	}
}
