package br.com.geraldoferraz.bks.listadesejo.api.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.geraldoferraz.bks.entidades.ListaDesejo;
import br.com.geraldoferraz.bks.listadesejo.api.conversor.Conversor;
import br.com.geraldoferraz.bks.listadesejo.api.dto.ListaDesejoDTO;
import br.com.geraldoferraz.bks.listadesejo.service.ListaDesejoService;

@RestController
@RequestMapping("/listasDesejo")
public class ListaDesejoController {

	@Autowired
	private Conversor conversor;

	@Autowired
	private ListaDesejoService service;

	@GetMapping("/")
	public List<ListaDesejoDTO> list() {
		return conversor.entidades(service.list()).converterLista();
	}

	@PostMapping
	public ResponseEntity<ListaDesejoDTO> create(@RequestBody ListaDesejoDTO listaDesjoDTO,
			UriComponentsBuilder uriBuilder) {
		ListaDesejo listaDesejo = conversor.dto(listaDesjoDTO).converter();
		listaDesejo = service.create(listaDesejo);

		URI uri = uriBuilder.path("/listasDesejo/{id}").buildAndExpand(listaDesejo.getId()).toUri();
		return ResponseEntity.created(uri).body(conversor.entidade(listaDesejo).converter());
	}

	@GetMapping("/{id}")
	public ResponseEntity<ListaDesejoDTO> retrieve(@PathVariable Long id) {
		return ResponseEntity.ok(conversor.entidade(service.retrieve(id)).converter());
	}

	@PutMapping("/{id}")
	public ResponseEntity<ListaDesejoDTO> update(@PathVariable Long id, @RequestBody ListaDesejoDTO listaDesejoDTO) {
		ListaDesejo entidade = conversor.dto(listaDesejoDTO).converter();
		return ResponseEntity.ok(conversor.entidade(service.update(id, entidade)).converter());
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> delete(@PathVariable Long id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}

}
