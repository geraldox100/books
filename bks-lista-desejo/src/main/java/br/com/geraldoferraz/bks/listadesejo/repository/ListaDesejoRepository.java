package br.com.geraldoferraz.bks.listadesejo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.geraldoferraz.bks.entidades.ListaDesejo;

public interface ListaDesejoRepository extends JpaRepository<ListaDesejo,Long>{

	Optional<ListaDesejo> findByNome(String nome);

}
