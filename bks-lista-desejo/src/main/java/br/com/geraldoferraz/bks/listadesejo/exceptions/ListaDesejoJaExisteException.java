package br.com.geraldoferraz.bks.listadesejo.exceptions;

import java.net.URI;

import org.springframework.web.util.UriComponentsBuilder;

import br.com.geraldoferraz.bks.comum.exceptions.DadoDuplicadoException;
import br.com.geraldoferraz.bks.entidades.ListaDesejo;

public class ListaDesejoJaExisteException extends DadoDuplicadoException {

	private static final long serialVersionUID = 1L;
	
	private String location;

	public ListaDesejoJaExisteException(String message) {
		super(message);
	}

	public ListaDesejoJaExisteException(ListaDesejo listaDesejo) {
		super("Lista " + listaDesejo.getNome() + " encontrador para id " + listaDesejo.getId());
		
		UriComponentsBuilder newInstance = UriComponentsBuilder.newInstance();
		
		URI uri = newInstance.path("/listasDesejo/{id}").buildAndExpand(listaDesejo.getId()).toUri();
		location = uri.toString();
	}

	@Override
	public String getLocation() {
		return location;
	}

	public static void throwMe(ListaDesejo l) {
		throw new ListaDesejoJaExisteException(l);
	}

}
