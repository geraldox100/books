package br.com.geraldoferraz.bks.listadesejo.api.conversor;

import java.util.List;

import org.springframework.stereotype.Component;

import br.com.geraldoferraz.bks.entidades.ListaDesejo;
import br.com.geraldoferraz.bks.listadesejo.api.dto.ListaDesejoDTO;

@Component
public class Conversor {

	public ConversorEntidade entidade(ListaDesejo entidade) {
		return new ConversorEntidade(entidade);
	}

	public ConversorEntidade entidades(List<ListaDesejo> entidades) {
		return new ConversorEntidade(entidades);
	}

	public ConversorDTO dto(ListaDesejoDTO dto) {
		return new ConversorDTO(dto);
	}

}
