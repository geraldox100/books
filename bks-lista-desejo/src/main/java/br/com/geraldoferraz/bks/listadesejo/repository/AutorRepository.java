package br.com.geraldoferraz.bks.listadesejo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.geraldoferraz.bks.entidades.Autor;

public interface AutorRepository extends JpaRepository<Autor,Long>{
	
	public Autor findByNome(String nome);

}
