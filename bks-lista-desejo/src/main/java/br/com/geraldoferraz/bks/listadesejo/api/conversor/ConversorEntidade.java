package br.com.geraldoferraz.bks.listadesejo.api.conversor;

import java.util.ArrayList;
import java.util.List;

import br.com.geraldoferraz.bks.entidades.ListaDesejo;
import br.com.geraldoferraz.bks.listadesejo.api.dto.ListaDesejoDTO;

public class ConversorEntidade {

	private List<ListaDesejo> entidades;
	private ListaDesejo entidade;

	public ConversorEntidade(ListaDesejo entidade) {
		this.entidade = entidade;
	}

	public ConversorEntidade(List<ListaDesejo> entidades) {
		this.entidades = entidades;
	}

	public List<ListaDesejoDTO> converterLista() {
		List<ListaDesejoDTO> dtos = new ArrayList<>();
		
		for (ListaDesejo entidade : entidades) {
			dtos.add(converter(entidade));
		}
		
		return dtos;
	}
	
	public ListaDesejoDTO converter() {
		return converter(entidade);
	}

	private ListaDesejoDTO converter(ListaDesejo entidade) {
		ListaDesejoDTO dto = new ListaDesejoDTO();
		
		dto.setId(entidade.getId());
		dto.setNome(entidade.getNome());
		dto.setDataCriacao(entidade.getDataCriacao());
		
		return dto;
	}


}
