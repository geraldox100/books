package br.com.geraldoferraz.bks.entidades.id;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ListaDesejoLivroId implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "idListaDesejo")
	private Long idListaDesejo;

	@Column(name = "idLivro")
	private Long idLivro;

	public Long getIdListaDesejo() {
		return idListaDesejo;
	}

	public void setIdListaDesejo(Long idListaDesejo) {
		this.idListaDesejo = idListaDesejo;
	}

	public Long getIdLivro() {
		return idLivro;
	}

	public void setIdLivro(Long idLivro) {
		this.idLivro = idLivro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idListaDesejo == null) ? 0 : idListaDesejo.hashCode());
		result = prime * result + ((idLivro == null) ? 0 : idLivro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ListaDesejoLivroId other = (ListaDesejoLivroId) obj;
		if (idListaDesejo == null) {
			if (other.idListaDesejo != null)
				return false;
		} else if (!idListaDesejo.equals(other.idListaDesejo))
			return false;
		if (idLivro == null) {
			if (other.idLivro != null)
				return false;
		} else if (!idLivro.equals(other.idLivro))
			return false;
		return true;
	}

}
