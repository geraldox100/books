package br.com.geraldoferraz.bks.entidades;

public enum TipoMeta {
	
	NAO_USAR(0,"NAO_USAR"),ANUAL(1,"ANUAL"),MENSAL(2,"MENSAL");

	private int codigo;
	private String descricao;

	TipoMeta(int codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public int getCodigo() {
		return codigo;
	}
	
	

}
