package br.com.geraldoferraz.bks.entidades.id;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SituacaoLivroId implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "codigoSituacao")
	private Integer codigoSituacao;

	@Column(name = "idLivro")
	private Long idLivro;

	public Integer getCodigoSituacao() {
		return codigoSituacao;
	}

	public void setCodigoSituacao(Integer codigoSituacao) {
		this.codigoSituacao = codigoSituacao;
	}

	public Long getIdLivro() {
		return idLivro;
	}

	public void setIdLivro(Long idLivro) {
		this.idLivro = idLivro;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigoSituacao == null) ? 0 : codigoSituacao.hashCode());
		result = prime * result + ((idLivro == null) ? 0 : idLivro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SituacaoLivroId other = (SituacaoLivroId) obj;
		if (codigoSituacao == null) {
			if (other.codigoSituacao != null)
				return false;
		} else if (!codigoSituacao.equals(other.codigoSituacao))
			return false;
		if (idLivro == null) {
			if (other.idLivro != null)
				return false;
		} else if (!idLivro.equals(other.idLivro))
			return false;
		return true;
	}

}
