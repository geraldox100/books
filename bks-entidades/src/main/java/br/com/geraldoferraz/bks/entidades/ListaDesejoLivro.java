package br.com.geraldoferraz.bks.entidades;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import br.com.geraldoferraz.bks.entidades.id.ListaDesejoLivroId;


@Entity
public class ListaDesejoLivro extends Entidade {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ListaDesejoLivroId id;

	@ManyToOne
	@MapsId("idListaDesejo")
	private ListaDesejo listaDesejo;

	@ManyToOne
	@MapsId("idLivro")
	private Livro livro;

	private String link;

	public ListaDesejoLivroId getId() {
		return id;
	}

	public void setId(ListaDesejoLivroId id) {
		this.id = id;
	}

	public ListaDesejo getListaDesejo() {
		return listaDesejo;
	}

	public void setListaDesejo(ListaDesejo listaDesejo) {
		this.listaDesejo = listaDesejo;
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
