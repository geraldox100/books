package br.com.geraldoferraz.bks.entidades;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Situacao extends Entidade{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Integer codigo;

	private String descricao;

	public Integer getCodigo() {
		return codigo;
	}
	
	@Override
	public Integer getId() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
