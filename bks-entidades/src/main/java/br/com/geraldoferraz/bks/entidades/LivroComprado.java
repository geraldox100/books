package br.com.geraldoferraz.bks.entidades;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class LivroComprado extends Livro {

	private static final long serialVersionUID = 1L;
	
	private Date dataCompra;
	
	@OneToMany(mappedBy = "livro")
	private List<Progresso> progressos;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "livroComprado")
	private List<SituacaoLivro> situacoes;

	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}

}
