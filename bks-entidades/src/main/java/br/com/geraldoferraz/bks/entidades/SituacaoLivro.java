package br.com.geraldoferraz.bks.entidades;


import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import br.com.geraldoferraz.bks.entidades.id.SituacaoLivroId;

@Entity
public class SituacaoLivro extends Entidade{

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private SituacaoLivroId id;

	@ManyToOne
	@JoinColumn(name = "codigoSituacao")
	@MapsId("codigoSituacao")
	private Situacao situacao;
	
	@ManyToOne
	@JoinColumn(name = "idLivro")
	@MapsId("idLivro")
	private LivroComprado livroComprado;
	
	private Date dataSituacao;


	public Situacao getSituacao() {
		return situacao;
	}

	public void setSituacao(Situacao situacao) {
		this.situacao = situacao;
	}

	public LivroComprado getLivroComprado() {
		return livroComprado;
	}

	public void setLivroComprado(LivroComprado livro) {
		this.livroComprado = livro;
	}

	public Date getDataSituacao() {
		return dataSituacao;
	}

	public void setDataSituacao(Date dataSituacao) {
		this.dataSituacao = dataSituacao;
	}

	@Override
	public SituacaoLivroId getId() {
		return id;
	}

}
