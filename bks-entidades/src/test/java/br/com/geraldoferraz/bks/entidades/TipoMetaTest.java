package br.com.geraldoferraz.bks.entidades;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class TipoMetaTest {

	@Test
	public void quandoAvaliarGetters() {
		assertAll("NAO_USAR", 
				() -> assertEquals(0, TipoMeta.NAO_USAR.getCodigo()),
				() -> assertEquals("NAO_USAR", TipoMeta.NAO_USAR.getDescricao()));
		
		assertAll("ANUAL", 
				() -> assertEquals(1, TipoMeta.ANUAL.getCodigo()),
				() -> assertEquals("ANUAL", TipoMeta.ANUAL.getDescricao()));
		
		assertAll("MENSAL", 
				() -> assertEquals(2, TipoMeta.MENSAL.getCodigo()),
				() -> assertEquals("MENSAL", TipoMeta.MENSAL.getDescricao()));
	}

}
