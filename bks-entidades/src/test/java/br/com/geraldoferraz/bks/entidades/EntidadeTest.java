package br.com.geraldoferraz.bks.entidades;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class EntidadeTest {
	
	private Entidade entidade;
	
	@BeforeEach
	public void beforeEachEntidadeTest() {
		entidade = getEntidade(1);
	}
	
	@Nested
	class QuandoComparar{
		
		@Nested
		class EntidadesDevmSerIguais{
			
			@Test
			public void seForemDoMesmoTipo() {
				assertTrue(entidade.equals(getEntidade(1)));
			}
			
			@Test
			public void seForemOMesmoObjeto() {
				assertTrue(entidade.equals(entidade));
			}
		}
		
		@Nested
		class EntidadesDevmSerDiferentes{
			
			@Test
			public void seNaoForemDoMesmoTipo() {
				assertFalse(entidade.equals("teste"));
			}
			
			@Test
			public void serDiferentesSeValorForNulo() {
				assertFalse(entidade.equals(null));
			}
			
			@Test
			public void serIdComparadoForNulo() {
				assertFalse(entidade.equals(getEntidade(null)));
			}
			
			@Test
			public void serIdEntidadeForNulo() {
				entidade = getEntidade(null);
				assertFalse(entidade.equals(getEntidade(1)));
			}
			
			@Test
			public void serIdCopmparadoEEntidadeForNulo() {
				entidade = getEntidade(null);
				assertFalse(entidade.equals(getEntidade(null)));
			}
		}
	}
	
	@Nested
	class QuandoAnalisarHashCode{
		
		@Test
		public void devePossuirValorMesmoComIdNulo() {
			entidade = getEntidade(null);
			assertThat(entidade.hashCode(), not(equalTo(null)));
		}
		
		@Nested
		class ValoresDevemSerIguais{
			
			@Test
			public void seOIdforIgual() {
				assertEquals(entidade.hashCode(), getEntidade(1).hashCode());
			}
		}
		
		@Nested
		class ValoresDevemSerDiferentes{
			
			@Test
			public void seOIdforDiferente() {
				assertNotEquals(entidade.hashCode(), getEntidade(2).hashCode());
			}
		}
		
		
	}

	private Entidade getEntidade(Integer id) {
		Entidade retorno = new Entidade() {

			private static final long serialVersionUID = 1L;

			@Override
			public Integer getId() {
				return id;
			}
			
		};
		return retorno;
	}

}
