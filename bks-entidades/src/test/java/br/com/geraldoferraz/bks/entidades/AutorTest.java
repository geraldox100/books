package br.com.geraldoferraz.bks.entidades;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class AutorTest {

	private static final String NOME = "autor";
	private static final long ID = 1l;
	private Autor autor;

	@BeforeEach
	public void beforeEachAutorTest() {
		autor = getAutor(ID,NOME);
	}
	
	@Test
	public void quandoExecutarConstrutorPassandoNomeGetterDeveRetornarValorDefinido() {
		String outrNome = "outro nome";
		Autor a = new Autor(outrNome);
		assertEquals(outrNome, a.getNome());
	}

	@Nested
	class QuandoExecutarSetters {

		@Test
		public void gettersDevemRetornarOValorDefinido() {
			assertAll("autor",
					()->assertEquals(ID, autor.getId()),
					()->assertEquals(NOME, autor.getNome()));
		}
		
		@Test
		public void toStringDeveRetornarValoresDefinidos() {
			assertEquals("Autor [id=" + ID + ", nome=" + NOME + "]", autor.toString());
		}

	}
	
	@Nested
	class QuandoCompararDoisAutores {

		@Test
		public void devemSerIguaisSeTiveremOMesmoId() {
			assertTrue(autor.equals(getAutor(ID,"outroNome")));
		}
		
		@Test
		public void devemSerDiferentesSeNaoTiveremOMesmoId() {
			assertFalse(autor.equals(getAutor(ID+2,"outroNome")));
		}

	}

	public Autor getAutor(long id, String nome) {
		Autor autor = new Autor();
		autor.setId(id);
		autor.setNome(nome);
		return autor;
	}

}
