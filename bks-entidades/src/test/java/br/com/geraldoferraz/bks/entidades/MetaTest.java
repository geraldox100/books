package br.com.geraldoferraz.bks.entidades;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class MetaTest {
	
	private static final String TITULO = "meta";
	private static final long ID = 1l;
	private Meta meta;
	private Date dataInicio;
	private Date dataFim;
	

	@BeforeEach
	public void beforeEachMetaTest() {
		meta = getMeta(ID,TITULO);
	}
	

	@Nested
	class QuandoExecutarSetters {

		@Test
		public void gettersDevemRetornarOValorDefinido() {
			assertAll("meta",
				()->assertEquals(ID, meta.getId()),
				()->assertEquals(TITULO, meta.getTitulo()),
				()->assertEquals(dataInicio, meta.getDataInicio()),
				()->assertEquals(dataFim, meta.getDataFim()),
				()->assertEquals(100, meta.getQuantidade()),
				()->assertEquals(TipoMeta.ANUAL, meta.getTipo()));
		}
		
		@Test
		public void toStringDeveRetornarValoresDefinidos() {
			assertEquals("Meta [id=1, titulo=meta, dataInicio="+dataInicio+", dataFim="+dataFim+", quantidade=100, tipo=ANUAL]", meta.toString());
		}

	}
	
	@Nested
	class QuandoCompararDoisMetaes {

		@Test
		public void devemSerIguaisSeTiveremOMesmoId() {
			assertTrue(meta.equals(getMeta(ID,"outroNome")));
		}
		
		@Test
		public void devemSerDiferentesSeNaoTiveremOMesmoId() {
			assertFalse(meta.equals(getMeta(ID+2,"outroNome")));
		}

	}

	public Meta getMeta(long id, String nome) {
		Meta meta = new Meta();
		meta.setId(id);
		meta.setTitulo(nome);
		meta.setDataInicio(dataInicio);
		meta.setDataFim(dataFim);
		meta.setQuantidade(100);
		meta.setTipo(TipoMeta.ANUAL);
		return meta;
	}

}
