package br.com.geraldoferraz.bks.entidades;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class CategoriaTest {
	
	private static final String NOME = "categoria";
	private static final long ID = 1l;
	private Categoria categoria;

	@BeforeEach
	public void beforeEachCategoriaTest() {
		categoria = getCategoria(ID,NOME);
	}
	
	@Test
	public void quandoExecutarConstrutorPassandoNomeGetterDeveRetornarValorDefinido() {
		String outrNome = "outro nome";
		Categoria a = new Categoria(outrNome);
		assertEquals(outrNome, a.getNome());
	}

	@Nested
	class QuandoExecutarSetters {

		@Test
		public void gettersDevemRetornarOValorDefinido() {
			assertEquals(ID, categoria.getId());
			assertEquals(NOME, categoria.getNome());
		}
		
		@Test
		public void toStringDeveRetornarValoresDefinidos() {
			assertEquals("Categoria [id=" + ID + ", nome=" + NOME + "]", categoria.toString());
		}

	}
	
	@Nested
	class QuandoCompararDoisCategoriaes {

		@Test
		public void devemSerIguaisSeTiveremOMesmoId() {
			assertTrue(categoria.equals(getCategoria(ID,"outroNome")));
		}
		
		@Test
		public void devemSerDiferentesSeNaoTiveremOMesmoId() {
			assertFalse(categoria.equals(getCategoria(ID+2,"outroNome")));
		}

	}

	public Categoria getCategoria(long id, String nome) {
		Categoria categoria = new Categoria();
		categoria.setId(id);
		categoria.setNome(nome);
		return categoria;
	}

}
