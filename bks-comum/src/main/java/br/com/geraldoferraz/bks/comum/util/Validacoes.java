package br.com.geraldoferraz.bks.comum.util;

import static br.com.geraldoferraz.bks.comum.util.Assercoes.isEqualLowerThan;
import static br.com.geraldoferraz.bks.comum.util.Assercoes.isNull;
import static br.com.geraldoferraz.bks.comum.util.Assercoes.isNullOrEmpty;

public final class Validacoes {
	
	private Validacoes() {}

	public static void mustNotBeNull(Object o, String nomeCampo) {
		if(isNull(o)) {
			throw new IllegalArgumentException("Campo "+nomeCampo+" não pode ser nulo");
		}
	}

	public static void mustNotBeNullOrEmpty(String valor, String nomeCampo) {
		if(isNullOrEmpty(valor)) {
			throw new IllegalArgumentException("Campo "+nomeCampo+" não pode ser nulo ou vazio");
		}
	}

	public static void mustBeGT(Number valor, Number comparacao, String nomeCampo) {
		if(isEqualLowerThan(valor,comparacao)) {
			throw new IllegalArgumentException("Valor "+nomeCampo+" deve ser maior que "+comparacao.doubleValue());
		}
	}

}
