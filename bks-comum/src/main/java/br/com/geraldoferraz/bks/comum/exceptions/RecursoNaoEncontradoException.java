package br.com.geraldoferraz.bks.comum.exceptions;

public class RecursoNaoEncontradoException extends RuntimeException implements JsonException {

	private static final long serialVersionUID = 1L;
	
	private static final String MENSAGEM_PADRAO = "Recurso não encontrado";
	
	
	public RecursoNaoEncontradoException() {
		super(MENSAGEM_PADRAO);
	}

	public RecursoNaoEncontradoException(String mensagem) {
		super(mensagem);
	}


	
}
