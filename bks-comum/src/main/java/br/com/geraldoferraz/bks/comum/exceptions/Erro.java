package br.com.geraldoferraz.bks.comum.exceptions;

public class Erro {

	private String error;
	private String message;
	
	public Erro(String error, String message) {
		this.error = error;
		this.message = message;
	}

	public Erro(RuntimeException ex) {
		this.error = ex.getClass().getSimpleName();
		this.message = ex.getMessage();
	}

	public String getError() {
		return error;
	}

	public String getMessage() {
		return message;
	}


}
