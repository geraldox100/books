package br.com.geraldoferraz.bks.comum.exceptions;

public abstract class DadoDuplicadoException extends RuntimeException implements JsonException {

	private static final long serialVersionUID = 1L;
	
	public DadoDuplicadoException(String message) {
		super(message);
	}
	
	public abstract String getLocation();


}
