package br.com.geraldoferraz.bks.comum.util;

public final class Assercoes {
	
	private Assercoes() {}
	
	public static boolean notNull(Object o) {
		return o != null;
	}

	public static boolean isNull(Object o) {
		return o == null;
	}

	public static boolean notNullOrEmpty(String s) {
		return notNull(s) && !s.isEmpty();
	}
	
	public static boolean isNullOrEmpty(String s) {
		return !notNullOrEmpty(s);
	}

	public static boolean isLowerThan(Number valor, Number comparacao) {
		return valor.doubleValue() < comparacao.doubleValue();
	}

	public static boolean isEqualLowerThan(Number valor, Number comparacao) {
		return valor.doubleValue() == comparacao.doubleValue() || valor.doubleValue() < comparacao.doubleValue();
	}

}
