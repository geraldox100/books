package br.com.geraldoferraz.bks.comum.exceptions;

import static br.com.geraldoferraz.bks.comum.exceptions.JsonExceptionEnum.MAPPER;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface JsonException {

	String getMessage();

	default String toJson() {
		try {
			return MAPPER.get().writeValueAsString(new Erro(getClass().getSimpleName(),getMessage()));
		} catch (JsonProcessingException e) {
			throw new RuntimeException();
		}
		
	}

}
