package br.com.geraldoferraz.bks.comum.exceptions;

import com.fasterxml.jackson.databind.ObjectMapper;

enum JsonExceptionEnum {

	MAPPER(new ObjectMapper());

	private ObjectMapper mapper;

	JsonExceptionEnum(ObjectMapper mapper) {
		this.mapper = mapper;
	}
	
	public ObjectMapper get() {
		return mapper;
	}
}
