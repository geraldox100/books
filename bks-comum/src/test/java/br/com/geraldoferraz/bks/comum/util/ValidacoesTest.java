package br.com.geraldoferraz.bks.comum.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ValidacoesTest {

	@Nested
	class QuandoExecutarMustNotBeNull {
		@Test
		public void passandoNullDeveLancarExcecao() {
			Assertions.assertThrows(IllegalArgumentException.class, () -> {
				Validacoes.mustNotBeNull(null, "not null object");
			});
		}

		@Test
		public void passandoValorValidoDevePassar() {
			Validacoes.mustNotBeNull("hello", "not null object");
		}

	}

	@Nested
	class QuandoExecutarMustNotBeNullOrEmpty {
		@Test
		public void passandoVazioDeveLancarExcecao() {
			Assertions.assertThrows(IllegalArgumentException.class, () -> {
				Validacoes.mustNotBeNullOrEmpty("", "not null object");
			});
		}

		@Test
		public void passandoNuloDeveLancarExcecao() {
			Assertions.assertThrows(IllegalArgumentException.class, () -> {
				String s = null;
				Validacoes.mustNotBeNullOrEmpty(s, "not null object");
			});
		}

		@Test
		public void passandoValorValidoDevePassar() {
			Validacoes.mustNotBeNullOrEmpty("hello", "not null object");
		}
	}

	@Nested
	class QuandoExecutarMustBeGT {
		@Test
		public void passandoValorMenorDeveLancarExcecao() {
			Assertions.assertThrows(IllegalArgumentException.class, () -> {
				Validacoes.mustBeGT(1, 2, "not null object");
			});
		}

		@Test
		public void passandoValorIgualDeveLancarExcecao() {
			Assertions.assertThrows(IllegalArgumentException.class, () -> {
				Validacoes.mustBeGT(2, 2, "not null object");
			});
		}

		@Test
		public void passandoValorMaiorDevePassar() {
			Validacoes.mustBeGT(3, 2, "not null object");
		}
	}

}
