package br.com.geraldoferraz.bks.comum.util;

import static br.com.geraldoferraz.bks.comum.util.Assercoes.isEqualLowerThan;
import static br.com.geraldoferraz.bks.comum.util.Assercoes.isLowerThan;
import static br.com.geraldoferraz.bks.comum.util.Assercoes.isNull;
import static br.com.geraldoferraz.bks.comum.util.Assercoes.isNullOrEmpty;
import static br.com.geraldoferraz.bks.comum.util.Assercoes.notNull;
import static br.com.geraldoferraz.bks.comum.util.Assercoes.notNullOrEmpty;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class AssercoesTest {

	@Nested
	class QuandoExecutarNotNull {

		@Test
		public void passandoNaoNuloDeveRetornarTrue() {
			assertTrue(notNull("not null object"));
		}

		@Test
		public void passandoNuloDeveRetornarFalse() {
			assertFalse(notNull(null));
		}

	}

	@Nested
	class QuandoExecutarIsNull {

		@Test
		public void passandoNaoNuloDeveRetornarFalse() {
			assertFalse(isNull("not null object"));
		}

		@Test
		public void passandoNullDeveRetornarTrue() {
			assertTrue(isNull(null));
		}

	}

	@Nested
	class QuandoExecutarNotNullOrEmpty {
		@Test
		public void passandoNaoNuloDeveRetornarTrue() {
			assertTrue(notNullOrEmpty("not null object"));
		}

		@Test
		public void passandoNuloDeveRetornarFalse() {
			String s = null;
			assertFalse(notNullOrEmpty(s));
		}

		@Test
		public void passandoVazioDeveRetornarFalse() {
			assertFalse(notNullOrEmpty(""));
		}
	}

	@Nested
	class QuandoExecutarIsNullOrEmpty {

		@Test
		public void passandoNaoNuloDeveRetornarFalse() {
			assertFalse(isNullOrEmpty("not null object"));
		}

		@Test
		public void passandoNuloDeveRetornarTrue() {
			String s = null;
			assertTrue(isNullOrEmpty(s));
		}

		@Test
		public void passandoVazioDeveRetornarTrue() {
			assertTrue(isNullOrEmpty(""));
		}
	}

	@Nested
	class QuandoExecutarIsLowerThan {

		@Test
		public void passandoValorMenorDeveRetornarTrue() {
			assertTrue(isLowerThan(5, 10));
		}

		@Test
		public void passandoValorMaiorDeveRetornarFalse() {
			assertFalse(isLowerThan(15, 10));
		}

		@Test
		public void passandoValorIgualDeveRetornarFalse() {
			assertFalse(isLowerThan(10, 10));
		}
	}

	@Nested
	class QuandoExecutarIsEqualLowerThan {

		@Test
		public void passandoValorMenorDeveRetornarTrue() {
			assertTrue(isEqualLowerThan(5, 10));
		}

		@Test
		public void passandoValorMaiorDeveRetornarFalse() {
			assertFalse(isEqualLowerThan(15, 10));
		}

		@Test
		public void passandoValorIgualDeveRetornarTure() {
			assertTrue(isEqualLowerThan(10, 10));
		}
	}

}
