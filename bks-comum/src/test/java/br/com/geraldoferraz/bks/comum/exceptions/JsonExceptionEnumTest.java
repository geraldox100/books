package br.com.geraldoferraz.bks.comum.exceptions;

import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonExceptionEnumTest {
	
	@Test
	public void verificaQueMapperNaoEstaNulo() {
		ObjectMapper mapper = JsonExceptionEnum.MAPPER.get();
		assertNotNull(mapper);
	}

}
