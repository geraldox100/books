package br.com.geraldoferraz.bks.comum.exceptions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.geraldoferraz.bks.comum.test.Injector;

@ExtendWith(MockitoExtension.class)
public class RecursoNaoEncontradoExceptionTest {

	@Mock
	private ObjectMapper mapper;

	@InjectMocks
	private RecursoNaoEncontradoException r;

	@Nested
	class QuandoConstruirRecursoNaoEncontradoException {
		@Test
		public void comConstrutorPadraoDeveProduzirJsonPadrao() {
			RecursoNaoEncontradoException r = new RecursoNaoEncontradoException();
			assertEquals("{\"error\":\"RecursoNaoEncontradoException\",\"message\":\"Recurso não encontrado\"}",r.toJson());
		}

		@Test
		public void comConstrutorRecebendoMensagemDeveProduzirJsonComAMensagem() {
			RecursoNaoEncontradoException r = new RecursoNaoEncontradoException("mensagem");
			assertEquals("{\"error\":\"RecursoNaoEncontradoException\",\"message\":\"mensagem\"}", r.toJson());
		}

		@Test
		public void jsonEstiverInvalidoDeveLancarExcecao() throws Exception {
			Injector.inject(JsonExceptionEnum.MAPPER, "mapper", mapper);
			when(mapper.writeValueAsString(Mockito.any())).thenThrow(JsonProcessingException.class);
			Assertions.assertThrows(RuntimeException.class, () -> {
				r.toJson();
			});
		}
	}

}
