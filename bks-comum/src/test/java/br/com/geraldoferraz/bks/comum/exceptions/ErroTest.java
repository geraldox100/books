package br.com.geraldoferraz.bks.comum.exceptions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class ErroTest {
	
	
	@Test
	public void quandConstruirPassandoValoresObjetoDeveSerPreenchido() {
		Erro e = new Erro("erro", "mensagem");
		assertEquals("erro", e.getError());
		assertEquals("mensagem", e.getMessage());
	}
	
	@Test
	public void quandoConstruirPassandoExcecaoObjetoDeveSerPreenchido() {
		Erro e = new Erro(new NullPointerException("mensagem"));
		assertEquals("NullPointerException", e.getError());
		assertEquals("mensagem", e.getMessage());
	}

}
