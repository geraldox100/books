package br.com.geraldoferraz.bks.comum.test;

import java.lang.reflect.Method;

import org.junit.jupiter.api.DisplayNameGenerator;


//TODO geraldo.filho se a classe ou metodo tiver @DisplayName deve dar preferencia 
public class CamelCaseDisplayNameGenarator implements DisplayNameGenerator{

	//https://www.baeldung.com/junit-custom-display-name-generator
	//https://mkyong.com/junit5/junit-5-display-names/
	
	@Override
	public String generateDisplayNameForClass(Class<?> testClass) {
		return splitAndReplaceCamelCase(testClass.getSimpleName());
	}

	@Override
	public String generateDisplayNameForNestedClass(Class<?> nestedClass) {
		return splitAndReplaceCamelCase(nestedClass.getSimpleName());
	}

	@Override
	public String generateDisplayNameForMethod(Class<?> testClass, Method testMethod) {
		return splitAndReplaceCamelCase(testMethod.getName());
	}
	
	
	//TODO geraldo.filho mover para um StringUtils
	public String splitAndReplaceCamelCase(String s) {
	   return s.replaceAll(
	      String.format("%s|%s|%s",
	         "(?<=[A-Z])(?=[A-Z][a-z])",
	         "(?<=[^A-Z])(?=[A-Z])",
	         "(?<=[A-Za-z])(?=[^A-Za-z])"
	      ),
	      " "
	   );
	}
	
	public String[] splitCamelCase(String s) {
		return s.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
	}

}
