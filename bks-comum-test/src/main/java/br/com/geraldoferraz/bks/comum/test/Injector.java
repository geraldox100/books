package br.com.geraldoferraz.bks.comum.test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class Injector {
	
	

	public static void inject(Class<?>clazz, String fieldName, Object mock) throws Exception {
		Field field;
		try {
			field = clazz.getField(fieldName);
		}catch (Exception e) {
			field = clazz.getDeclaredField(fieldName);
		}
		
		field.setAccessible(true);

		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

		field.set(null, mock);
	}
	
	public static void inject(Object instance, String fieldName, Object mock) throws Exception {
		Field field;
		try {
			field = instance.getClass().getField(fieldName);
		}catch (Exception e) {
			field = instance.getClass().getDeclaredField(fieldName);
		}
		
		field.setAccessible(true);

		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

		field.set(instance, mock);
	}
	

}
