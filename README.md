# books


[![pipeline status](https://gitlab.com/geraldox100/books/badges/feature-cobertura/pipeline.svg)](https://gitlab.com/geraldox100/books/-/commits/feature-cobertura)
[![coverage report](https://gitlab.com/geraldox100/books/badges/feature-cobertura/coverage.svg)](https://gitlab.com/geraldox100/books/-/commits/feature-cobertura)

projeto destinado ao estudo de arquitetura de micro-serviços
o aplicativo irá permitir que o cadastro de livros e wishlists, definir metas e monitorar progresso de leitura
este projeto será mono repo